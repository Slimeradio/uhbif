package helpers

import javafx.application.Application
import org.fxmisc.richtext.CodeArea
import org.fxmisc.richtext.LineNumberFactory
import org.fxmisc.richtext.model.StyleSpans
import org.fxmisc.richtext.model.StyleSpansBuilder
import java.util.Collections.emptyList
import java.util.Collections.singleton
import java.util.regex.Pattern

/*
* copy from https://github.com/FXMisc/RichTextFX/blob/master/richtextfx-demos/src/main/java/org/fxmisc/richtext/demo/XMLEditorDemo.java
*/

private val XML_TAG = Pattern.compile("(?<ELEMENT>(</?\\h*)(\\w+)([^<>]*)(\\h*/?>))" + "|(?<COMMENT><!--[^<>]+-->)")

private val ATTRIBUTES = Pattern.compile("(\\w+\\h*)(=)(\\h*\"[^\"]+\")")

private const val GROUP_OPEN_BRACKET = 2
private const val GROUP_ELEMENT_NAME = 3
private const val GROUP_ATTRIBUTES_SECTION = 4
private const val GROUP_CLOSE_BRACKET = 5
private const val GROUP_ATTRIBUTE_NAME = 1
private const val GROUP_EQUAL_SYMBOL = 2
private const val GROUP_ATTRIBUTE_VALUE = 3

fun CodeArea.highlightXML(appClass: Class<out Application>): CodeArea {
    return apply {
        this.paragraphGraphicFactory = LineNumberFactory.get(this)

        this.textProperty().addListener { obs, oldText, newText -> this.setStyleSpans(0, computeHighlightingXML(newText)) }

        this.stylesheets.add(appClass.getResource("xml-highlighting.css").toExternalForm())
    }
}

private fun computeHighlightingXML(text: String): StyleSpans<Collection<String>> {

    val matcher = XML_TAG.matcher(text)
    var lastKwEnd = 0
    val spansBuilder = StyleSpansBuilder<Collection<String>>()
    while (matcher.find()) {

        spansBuilder.add(emptyList(), matcher.start() - lastKwEnd)
        if (matcher.group("COMMENT") != null) {
            spansBuilder.add(singleton("comment"), matcher.end() - matcher.start())
        } else {
            if (matcher.group("ELEMENT") != null) {
                val attributesText = matcher.group(GROUP_ATTRIBUTES_SECTION)

                spansBuilder.add(singleton("tagmark"), matcher.end(GROUP_OPEN_BRACKET) - matcher.start(GROUP_OPEN_BRACKET))
                spansBuilder.add(singleton("anytag"), matcher.end(GROUP_ELEMENT_NAME) - matcher.end(GROUP_OPEN_BRACKET))

                if (!attributesText.isEmpty()) {

                    lastKwEnd = 0

                    val amatcher = ATTRIBUTES.matcher(attributesText)
                    while (amatcher.find()) {
                        spansBuilder.add(emptyList(), amatcher.start() - lastKwEnd)
                        spansBuilder.add(singleton("attribute"), amatcher.end(GROUP_ATTRIBUTE_NAME) - amatcher.start(GROUP_ATTRIBUTE_NAME))
                        spansBuilder.add(singleton("tagmark"), amatcher.end(GROUP_EQUAL_SYMBOL) - amatcher.end(GROUP_ATTRIBUTE_NAME))
                        spansBuilder.add(singleton("avalue"), amatcher.end(GROUP_ATTRIBUTE_VALUE) - amatcher.end(GROUP_EQUAL_SYMBOL))
                        lastKwEnd = amatcher.end()
                    }
                    if (attributesText.length > lastKwEnd)
                        spansBuilder.add(emptyList(), attributesText.length - lastKwEnd)
                }

                lastKwEnd = matcher.end(GROUP_ATTRIBUTES_SECTION)

                spansBuilder.add(singleton("tagmark"), matcher.end(GROUP_CLOSE_BRACKET) - lastKwEnd)
            }
        }
        lastKwEnd = matcher.end()
    }
    spansBuilder.add(emptyList(), text.length - lastKwEnd)
    return spansBuilder.create()
}