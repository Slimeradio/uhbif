package helpers

import java.io.*
import java.nio.charset.Charset
import java.text.ParseException
import kotlin.math.max

class CSVReader(var reader: BufferedReader, delim: Char = '|', val headerRowCount: Int = 1) : ITableReader {
    var delimiter = delim
    private lateinit var table: Table

    constructor(f: File, delim: Char = '|', charset: Charset = Charsets.UTF_8): this(FileInputStream(f).bufferedReader(charset), delim)

    override fun get(idx: Int): Table {
        if(!this::table.isInitialized){
            table = Table(reader.lineSequence(), headerRowCount)
        }
        return table
    }

    override fun iterator(): Iterator<Table> {
        return object : Iterator<Table>{
            var first: Boolean = true
            override fun hasNext(): Boolean = first

            override fun next(): Table {
                if(first){
                    first = false
                    if(!this@CSVReader::table.isInitialized){
                        table = Table(reader.lineSequence(), headerRowCount)
                    }
                    return table
                } else {
                    throw NoSuchElementException("single!!")
                }
            }

        }
    }

    override fun close() {
        reader.close()
    }

    private fun processRow(lines: Iterator<String>, sbRow: StringBuilder, sb: StringBuilder): List<Any?> {
        var ret: List<Any?>?
        sbRow.setLength(0)
        while (lines.hasNext()){
            if(sbRow.isNotEmpty()){
                sbRow.append("\r\n")
            }
            sbRow.append(lines.next())
            ret = processCols(sbRow, sb)
            if(ret != null){
                return ret
            }
        }
        throw ParseException("undefined end of string", -1)
    }

    private fun processCols(line: CharSequence, sb: StringBuilder): List<Any?>?{
        var quotes = false
        var mayInlineQuote = false
        val values = ArrayList<String>()
        sb.setLength(0)
        for (c in line) {
            if(quotes){
                if(c == '"'){
                    quotes = false
                    mayInlineQuote = true
                } else {
                    sb.append(c)
                }
            } else {
                when (c) {
                    '"' -> when {
                        mayInlineQuote -> {
                            quotes = true
                            sb.append('"')
                        }
                        sb.isEmpty() -> quotes = true
                        else -> sb.append('"')
                    }
                    delimiter -> {
                        values.add(sb.toString())
                        sb.setLength(0)
                    }
                    else -> sb.append(c)
                }
                mayInlineQuote = false
            }
        }
        return if(quotes){
            null
        } else {
            values.add(sb.toString())
            values.mapIndexed { i, s -> prepare(i, s) }
        }
    }

    private fun prepare(idx: Int, str: String): Any?{
        return when{
            str.isEmpty() || str == "null" -> null
            str == "true" || str == "false" -> str.toBoolean()
            str.contains(numReg) ->
                if(str.contains('.')){
                    str.toDoubleOrNull() ?: str
                } else {
                    str.toLongOrNull() ?: str
                }
            else -> str
        }
    }

    inner class Table(lines: Sequence<String>, headerRowCount: Int = 1): ITableReader.Table{
        var header: MutableList<String>
        val sb = StringBuilder()
        val sbRow = StringBuilder()
        val lineIterator: Iterator<String> = lines.iterator()

        init {
            if(headerRowCount > 0) {
                header = processRow(lineIterator, sb, sbRow).map { it.toString() }.toMutableList()
                if(headerRowCount > 1){
                    for(i in 1 until headerRowCount){
                        var lastHeader = header.first()
                        val lheader = processRow(lineIterator, sb, sbRow).map { it.toString() }
                        for ((j, item) in lheader.withIndex()) {
                            if(j == header.size){
                                header.add("null")
                            }
                            val startIndex = max(0, header[j].lastIndexOf(delimiter))
                            if(header[j].substring(startIndex) != "null"){//TODO "are this\tnull"???
                                lastHeader = header[j]
                            }
                            header[j] = lastHeader + delimiter + item
                        }
                    }
                }
            } else {
                header = ArrayList(0)
            }
        }
        override fun name(): String? = null

        override fun getHeader(): Array<String> = header.toTypedArray()

        override fun setHeader(arr: Array<String>) {
            header.clear()
            header.addAll(arr)
        }

        override fun iterator(): Iterator<Map<String, Any?>> {
            return object : Iterator<Map<String, Any?>> {
                override fun hasNext(): Boolean = lineIterator.hasNext()

                override fun next(): Map<String, Any?> {
                    val list = processRow(lineIterator, sb, sbRow)
                    return header.mapIndexed { i, key -> key to list[i] }.toMap(HashMap(header.size))
                }
            }
        }
    }

    companion object {
        val numReg = Regex("^[+\\-]?(0|[1-9]\\d*)(\\.(0|\\d*[1-9]))?$")
    }
}

open class CSVWriter(
        protected var output: BufferedWriter,
        protected val statHeader: Array<String>,
        protected val delimiter: Char = '|'): ITableWriter {
    private val anyQuoted = arrayOf(delimiter, ',', '\r', '\n').toCharArray()
    protected var tableInited = false
    protected var header = statHeader

    constructor(out: File, header: Array<String>, delimiter: Char = '|', charset: Charset = Charsets.UTF_8): this(FileOutputStream(out).bufferedWriter(charset), header, delimiter)

    override fun startTable(idx: Int, name: String?, header: Array<String>) {
        if(statHeader.isEmpty()){
            this.header = header
        }
        initTable(name)
    }

    override fun putRow(idx: Int, map: Map<String, Any?>) {
        var first = true
        for(h in header){
            if(first){
                output.newLine()
                first = false
            } else {
                output.append(delimiter)
            }
            output.append(prepareValue(map[h], anyQuoted))
        }
    }

    override fun flush() {
        output.flush()
    }

    override fun close() {
        output.close()
    }

    protected fun initTable(name: String?){
        if(!tableInited){
            var first = true
            for (h in header) {
                if(first){
                    first = false
                } else {
                    output.append(delimiter)
                }
                output.append(prepareValue(h, anyQuoted))
            }
            tableInited = true
        }
    }

    companion object {

        fun prepareValue(o: Any?, quotedChars: CharArray): String{
            return when(o){
                null -> ""
                is Boolean, is Int, is Long -> o.toString()
                is Float, is Double -> '"' + o.toString() + '"'
                else -> {
                    val s = o.toString()
                    when {
                        s.indexOf('"') >= 0 -> '"' + s.replace("\"", "\"\"") + '"'
                        s.indexOfAny(quotedChars) >= 0 -> '"' + s + '"'
                        else -> s
                    }
                }
            }
        }
    }
}

inline fun <T: ITableReaderTarget> T.fromCSV(f: File, delimiter: Char = '|', charset: Charset = Charsets.UTF_8): T {
    return from(CSVReader(f, delimiter, charset))
}

inline fun <T: ITableWriterSource> T.toCSV(f: File, header: Array<String> = emptyArray(), delimiter: Char = '|'): T {
    f.createNewFile()
    return to(CSVWriter(f, header, delimiter))
}