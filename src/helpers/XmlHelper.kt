package helpers

import com.sun.org.apache.xml.internal.serialize.Method
import com.sun.org.apache.xml.internal.serialize.OutputFormat
import com.sun.org.apache.xml.internal.serialize.XMLSerializer
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.xml.sax.InputSource
import org.xml.sax.SAXParseException
import java.io.IOException
import java.io.OutputStream
import java.io.StringReader
import java.io.StringWriter
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.TransformerException
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

val documentFactory = DocumentBuilderFactory.newInstance()

inline fun parseXML(string: String) = try{
    documentFactory.newDocumentBuilder().parse(InputSource(StringReader(string)))
} catch (e: SAXParseException){
    println("cannot parse xml:\n$string")
    throw e
}

inline fun emptyDocument() = documentFactory.newDocumentBuilder().newDocument()

inline fun Element.child(name: String) = this.getElementsByTagName(name).let {
    if(it.length > 0){
        it.item(0) as Element
    } else {
        null
    }
}

inline fun Element.children(name: String) = this.getElementsByTagName(name).let {
    object : Iterator<Element>{
        var idx = 0
        override fun hasNext(): Boolean = idx < it.length

        override fun next(): Element {
            idx++
            return it.item(idx - 1) as Element
        }
    }
}

inline fun Node.eachChildren(block: (Node) -> Unit){
    childNodes.run {
        for (i in 0 until length){
            block(item(i))
        }
    }
}

inline fun Node.firstChildren(predicate: (Node) -> Boolean): Node?{
    childNodes.run {
        for (i in 0 until length){
            val item = item(i)
            if(predicate(item)){
                return item
            }
        }
    }
    return null
}

inline fun<reified T: Node> Node.firstChildrenByType(predicate: (T) -> Boolean): T?{
    childNodes.run {
        for (i in 0 until length){
            val item = item(i)
            if(item is T && predicate(item)){
                return item
            }
        }
    }
    return null
}

inline fun <T> Element.attr(name: String, parse: (String) -> T): T? {
    return if(this.hasAttribute(name)) {
        this.getAttribute(name)?.let(parse)
    } else {
        null
    }
}

inline fun <T> Element.value(parse: (String) -> T) = this.textContent.let(parse)//???

inline fun Element.value() = this.textContent///???

inline fun Element.text() = this.textContent ?: ""

inline fun Element.addChildren(block: Document.() -> Iterable<Element>): Element = apply {
    val children = ownerDocument.block()
    for (child in children) {
        appendChild(child)
    }
}

inline fun Document.element(name: String) = createElement(name)

inline fun <T: Node> Document.copy(e: T, deep: Boolean = true) = importNode(e, deep) as T

inline fun <T> Element.attr(name: String, value: T, converter: (T) -> String? = { it?.toString() }): Element = apply {
    converter(value)?.also {
        this.setAttribute(name, it)
    }
}

inline fun <T> Element.value(value: T, converter: (T) -> String? = { it?.toString() }): Element = apply {
    this.textContent = converter(value)
}

inline fun <T: Node> T.appendTo(root: Element): T = apply {
    root.appendChild(this)
}

fun Node.write(os: OutputStream) {
    try {
        val tr = TransformerFactory.newInstance().newTransformer()
        val source = DOMSource(this)
        val result = StreamResult(os)
        tr.transform(source, result)
    } catch (e: TransformerException) {
        e.printStackTrace(System.out)
    } catch (e: IOException) {
        e.printStackTrace(System.out)
    }
}

fun Node.write2(): String{
    try {
        val format = OutputFormat(Method.XML, null, false)
        format.lineWidth = 0
        format.indenting = true
        format.indent = 2
        val out = StringWriter()
        val serializer = XMLSerializer(out, format)
        serializer.serialize(this)

        return out.toString()
    } catch (e: IOException) {
        e.printStackTrace()
        return ""
    }

}