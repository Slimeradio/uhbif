package helpers

import java.io.Serializable

data class MPair<A, B>(
        var first: A,
        var second: B
): Serializable{

    constructor(p: Pair<A, B>): this(p.first, p.second)

    override fun toString(): String = "($first, $second)"

    fun immutable() = first to second
}

fun <A, B> Pair<A, B>.mutable() = MPair(first, second)