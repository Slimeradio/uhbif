package sample

import helpers.CSVReader
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.MenuItem
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.input.Clipboard
import java.net.URL
import java.util.*

class UpdateItemsComponentsDialog: Initializable {

    @FXML
    lateinit var bal: TableView<Bal>

    @FXML
    lateinit var ImportBalClipboard: MenuItem

    @FXML
    lateinit var btnApply: Button

    lateinit var listener: (blockTypes: List<Bal>) -> Unit

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        val bal2ColCount = bal.columns.size
        bal.columns.apply {
            (find { it.id == "BLOCK" } as TableColumn<Bal, String>).setCellValueFactory{ obj -> obj!!.value.name }
        }
        ImportBalClipboard.setOnAction {
            val csvReader = CSVReader(Clipboard.getSystemClipboard().string.reader().buffered(), '\t')
            csvReader.first().apply {
                bal.items.clear()
                val header = getHeader()
                bal.columns.subList(bal2ColCount, bal.columns.size).clear()
                /*val splitHeader = header.copyOfRange(bal2ColCount, header.size).groupBy { it.split('\t').dropLast(1).joinToString("\t") { it } }.mapValues { it.value.map { it.split('\t').last() } }
                for(sh in splitHeader){
                    bal.columns.add(bal2Cell(sh.key, sh.value))
                }*/
                var last = ""
                var j = 0
                val prepHeader = header.copyOfRange(bal2ColCount, header.size).map {
                    if(it != "null"){
                        last = it
                        j = 0
                    }
                    last to when(j++){
                        0 -> STR_FULL_COUNT
                        1 -> STR_PRIORITY
                        2 -> STR_ADDITIONAL
                        3 -> STR_ADDITIONAL_PRIORITY
                        else -> ""
                    }
                }
                setHeader(arrayOf(*header.copyOfRange(0, bal2ColCount), *prepHeader.map { "${it.first}\t${it.second}" }.toTypedArray()))
                val splitHeader = prepHeader.groupBy { it.first }.mapValues { it.value.map { it.second } }
                for(sh in splitHeader){
                    bal.columns.add(bal2Cell(sh.key, sh.value))
                }
                bal.items.addAll(this.map {
                    Bal().apply {
                        name.value = it[header[0]]?.toString()
                        for(sh in splitHeader){
                            (companents[sh.key] ?: Bal.BalItem().apply { companents[sh.key] = this }).apply {
                                fullCount.value = (it["${sh.key}\t$STR_FULL_COUNT"] as? Number ?: 0).toInt()
                                priority.value = (it["${sh.key}\t$STR_PRIORITY"] as? Number ?: 0).toInt()
                                additional.value = (it["${sh.key}\t$STR_ADDITIONAL"] as? Number ?: 0).toInt()
                                additionalPriority.value = (it["${sh.key}\t$STR_ADDITIONAL_PRIORITY"] as? Number ?: 0).toInt()
                            }
                        }
                    }
                })
                println()
            }
            if(bal.items.isNotEmpty()){
                btnApply.isDisable = false
            }
        }
        btnApply.setOnAction {
            listener(bal.items)
        }
    }

    fun bal2Cell(name: String, names: List<String>): TableColumn<Bal, Bal.BalItem> = TableColumn<Bal, Bal.BalItem>(name).apply {
        names.filter { it in arrayOf(STR_FULL_COUNT, STR_PRIORITY, STR_ADDITIONAL, STR_ADDITIONAL_PRIORITY) }.map {
            TableColumn<Bal, Number>(it).apply {
                when(it) {
                    STR_FULL_COUNT -> {
                        setCellValueFactory { obj -> obj.value.companents[name]!!.fullCount }
                    }
                    STR_PRIORITY -> {
                        setCellValueFactory { obj -> obj.value.companents[name]!!.priority }
                    }
                    STR_ADDITIONAL -> {
                        setCellValueFactory { obj -> obj.value.companents[name]!!.additional }
                    }
                    STR_ADDITIONAL_PRIORITY -> {
                        setCellValueFactory { obj -> obj.value.companents[name]!!.additionalPriority }
                    }
                }
            }
        }.let { columns.addAll(it) }
    }

    class Bal{
        val name = SimpleStringProperty(this, "BLOCK")
        val companents = FXCollections.observableHashMap<String, BalItem>()

        class BalItem{
            val fullCount = SimpleIntegerProperty(this, STR_FULL_COUNT)
            val priority = SimpleIntegerProperty(this, STR_PRIORITY)
            val additional = SimpleIntegerProperty(this, STR_ADDITIONAL)
            val additionalPriority = SimpleIntegerProperty(this, STR_ADDITIONAL_PRIORITY)
        }
    }

    companion object {
        const val STR_FULL_COUNT = "К-Во всего"
        const val STR_PRIORITY = "Приоритет"
        const val STR_ADDITIONAL = "В усиление"
        const val STR_ADDITIONAL_PRIORITY = "Приоритет Усиления"
    }
}