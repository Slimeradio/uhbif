package sample

import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.MenuItem
import java.net.URL
import java.util.*

class MenuController: Initializable {
    lateinit var listener: IListener

    @FXML
    lateinit var impFromClipboard: MenuItem

    @FXML
    lateinit var expToClipboard: MenuItem

    @FXML
    lateinit var updateItemsComponents: MenuItem

    @FXML
    lateinit var updateItems: MenuItem

    @FXML
    lateinit var getItemsValues: MenuItem

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        impFromClipboard.setOnAction { listener.onAction(IMPORT_FROM_CLIPBOARD) }
        expToClipboard.setOnAction { listener.onAction(EXPORT_TO_CLIPBOARD) }
        updateItemsComponents.setOnAction { listener.onAction(UPDATE_ITEMS_COMPONENTS) }
        updateItems.setOnAction { listener.onAction(UPDATE_ITEMS) }
        getItemsValues.setOnAction { listener.onAction(GET_ITEMS_VALUES) }
    }

    interface IListener{
        fun onAction(item: Int)
    }

    companion object {
        const val IMPORT_FROM_CLIPBOARD = 1
        const val EXPORT_TO_CLIPBOARD = 2
        const val UPDATE_ITEMS_COMPONENTS = 100
        const val UPDATE_ITEMS = 101
        const val GET_ITEMS_VALUES = 102
    }
}
