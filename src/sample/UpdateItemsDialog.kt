package sample

import helpers.CSVReader
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.MenuItem
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.input.Clipboard
import java.net.URL
import java.util.*

class UpdateItemsDialog: Initializable {

    @FXML
    lateinit var bal: TableView<Bal>

    @FXML
    lateinit var ImportBalClipboard: MenuItem

    @FXML
    lateinit var btnApply: Button

    lateinit var listener: (blockTypes: List<Bal>) -> Unit

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        val balColCount = bal.columns.size
        bal.columns.apply {
            (find { it.id == "blockId" } as TableColumn<Bal, String>).setCellValueFactory{ obj -> obj!!.value.blockId }
        }
        ImportBalClipboard.setOnAction {
            val csvReader = CSVReader(Clipboard.getSystemClipboard().string.reader().buffered(), '\t')
            csvReader.first().apply {
                bal.items.clear()
                val header = getHeader()
                bal.columns.subList(balColCount, bal.columns.size).clear()
                for(i in balColCount until header.size){
                    bal.columns.add(bal2Cell(header[i]))
                }
                bal.items.addAll(this.mapNotNull {
                    if(it[header[0]] != null) {
                        Bal().apply {
                            blockId.value = it[header[0]]?.toString()
                            for (i in balColCount until header.size) {
                                (companents[header[i]]
                                        ?: SimpleStringProperty(this, header[i]).apply { companents[header[i]] = this }).value = it[header[i]]?.toString()
                            }
                        }
                    } else {
                        null
                    }
                })
            }
            if(bal.items.isNotEmpty()){
                btnApply.isDisable = false
            }
        }

        btnApply.setOnAction {
            listener(bal.items)
        }
    }

    fun bal2Cell(name: String): TableColumn<Bal, String> = TableColumn<Bal, String>(name).apply {
        setCellValueFactory { obj -> obj.value.companents[name] }
    }

    class Bal{
        val blockId = SimpleStringProperty(this, "blockId")
        val companents = FXCollections.observableHashMap<String, SimpleStringProperty>()
    }
}