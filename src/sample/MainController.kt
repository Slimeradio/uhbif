package sample

import helpers.*
import javafx.collections.ObservableList
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.cell.TextFieldTreeCell
import javafx.scene.input.Clipboard
import javafx.scene.input.DataFormat
import javafx.scene.layout.BorderPane
import javafx.scene.paint.Color
import javafx.stage.Stage
import javafx.util.StringConverter
import org.w3c.dom.Element
import sample.MenuController.Companion.EXPORT_TO_CLIPBOARD
import sample.MenuController.Companion.GET_ITEMS_VALUES
import sample.MenuController.Companion.IMPORT_FROM_CLIPBOARD
import sample.MenuController.Companion.UPDATE_ITEMS
import sample.MenuController.Companion.UPDATE_ITEMS_COMPONENTS
import sample.data.Block
import java.io.ByteArrayOutputStream
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class MainController: Initializable, MenuController.IListener {

    @FXML
    lateinit var root: BorderPane

    @FXML
    lateinit var XmlTree: TreeView<Element>

    lateinit var menuController: MenuController
    lateinit var editorController: EditorController
    var document = emptyDocument().apply {
        appendChild(
                element("Definitions")
                        .attr("xmlns:xsd", "http://www.w3.org/2001/XMLSchema")
                        .attr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                        .addChildren { listOf(element("CubeBlocks")) }
        )
    }
    var blocks = HashMap<Element, Block>()
    //val blocks = ArrayList<Block>(0)


    override fun initialize(location: URL?, resources: ResourceBundle?) {
        val menuLoader = FXMLLoader(javaClass.getResource("menu.fxml"))
        root.top = menuLoader.load()
        menuController = menuLoader.getController()
        menuController.listener = this
        val editorLoader = FXMLLoader(javaClass.getResource("editor.fxml"))
        root.center = editorLoader.load()
        editorController = editorLoader.getController()
        XmlTree.setCellFactory {
            TextFieldTreeCell<Element>().apply {
                converter = elementConverter
                treeItemProperty().addListener { _, oldValue, newValue ->
                    if((oldValue as? ElementItem)?.type != (newValue as? ElementItem)?.type){
                        contextMenu = when((newValue as? ElementItem)?.type ?: -1){
                            TYPE_DEFINITION -> createDefinitionMenu(this)
                            TYPE_DEFINITION_PARENT -> createDefinitionParentMenu(this)
                            else -> null
                        }
                    }
                }
            }
        }
        root.center.isVisible = false
        XmlTree.selectionModel.selectionMode = SelectionMode.MULTIPLE
        XmlTree.selectionModel.selectedItemProperty().addListener { _, oldValue, newValue ->
            if(oldValue != null){
                if(oldValue !is ElementItem) {
                    throw IllegalStateException("${oldValue::class.java.name} is ot in ${ElementItem::class.java.name}")
                }
                if(oldValue.type == TYPE_DEFINITION){

                }
            }
            if(newValue != null) {
                if (newValue !is ElementItem) {
                    throw IllegalStateException("${newValue::class.java.name} is ot in ${ElementItem::class.java.name}")
                }
                if (newValue.type == TYPE_DEFINITION) {
                    val element = newValue.value
                    try{
                        val block = blocks[element] ?: Block.fromXML(element).apply { blocks[element] = this }
                        editorController.changeBlock(block)
                        root.center.isVisible = true
                    } catch (e: Exception){
                        System.out.println("Exception from parse / move to editor block ${getElementName(element)}")
                        e.printStackTrace()
                        root.center.isVisible = false
                    }
                } else {
                    root.center.isVisible = false
                }
            } else {
                root.center.isVisible = false
            }
        }
        XmlTree.root = document.firstChildren{
            it is Element
        }?.let { createElementItem(it as Element) }
    }

    override fun onAction(item: Int) {
        when(item){
            IMPORT_FROM_CLIPBOARD -> {
                importFromString(Clipboard.getSystemClipboard().string ?: "<null_document />")
            }
            EXPORT_TO_CLIPBOARD -> {
                Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to exportToString()))
            }
            UPDATE_ITEMS_COMPONENTS -> {
                val loader = FXMLLoader(javaClass.getResource("UpdateItemsComponentsDialog.fxml"))
                val root = loader.load<Parent>()
                val stage = Stage()
                loader.getController<UpdateItemsComponentsDialog>().listener = { blockTypes ->
                    var needUpdateEditor = false
                    val targetBlocksMap = blockTypes.map {
                        it.name.value to it
                    }.toMap()
                    (document.firstChildrenByType<Element> { it.tagName == "Definitions" } ?: document)
                            ?.firstChildrenByType<Element> { it.tagName == "CubeBlocks" }
                            ?.eachChildren {
                                (it as? Element)?.takeIf { it.tagName == "Definition" }?.also {
                                    val block = blocks[it] ?: Block.fromXML(it).apply { blocks[it] = this }
                                    val blockId = block.blockId?.let { it.blockSubtypeId.takeUnless { it.isNullOrBlank() } ?: it.blockId } ?: ""
                                    if(blockId.isNotBlank() && targetBlocksMap.containsKey(blockId)){
                                        val bal = targetBlocksMap.getValue(blockId)
                                        val sortedOrdersFunc = bal.companents.mapNotNull {
                                            when{
                                                it.value.fullCount.value <= 0 -> null
                                                it.value.additional.value >= it.value.fullCount.value -> null
                                                else -> it
                                            }
                                        }.sortedBy { it.value.priority.value }
                                        val sortedOrdersExtra = bal.companents.mapNotNull {
                                            when{
                                                it.value.fullCount.value <= 0 -> null
                                                it.value.additional.value <= 0 -> null
                                                else -> it
                                            }
                                        }.sortedBy { it.value.additionalPriority.value }
                                        val newSchema = ArrayList<Pair<String, Int>>(sortedOrdersFunc.size + sortedOrdersExtra.size)
                                        sortedOrdersFunc.forEach {
                                            newSchema += it.key to it.value.let { it.fullCount.value - it.additional.value }
                                        }
                                        val newCriticalIdx = newSchema.size - 1
                                        sortedOrdersExtra.forEach {
                                            newSchema += it.key to it.value.additional.value
                                        }
                                        (block.component ?: ArrayList<Block.Companion.ComponentItem>().apply { block.component = this }).apply {
                                            clear()
                                            addAll(newSchema.map {
                                                Block.Companion.ComponentItem(it.second, it.first)
                                            })
                                        }
                                        block.criticalComponent = newCriticalIdx.takeIf { it >= 0 }?.let { MPair(newSchema[it].first, 0) }
                                        if(editorController.getBlock() == block){
                                            needUpdateEditor = true
                                        }
                                    }
                                }
                            }
                    if(needUpdateEditor){
                        editorController.blockExternalChanged()
                    }
                    stage.hide()
                }
                stage.title = "SE Block Editor - change blocks components"
                stage.scene = Scene(root, 950.0, 600.0)
                stage.showAndWait()
            }
            UPDATE_ITEMS -> {
                val loader = FXMLLoader(javaClass.getResource("UpdateItemsDialog.fxml"))
                val root = loader.load<Parent>()
                val stage = Stage()
                loader.getController<UpdateItemsDialog>().listener = { blockTypes ->
                    var needUpdateEditor = false

                    val targetBlocksMap = blockTypes.map {
                        it.blockId.value to it
                    }.toMap()

                    val editors = editorController.inputControllers.map {
                        it.name to it
                    }.toMap()

                    document.firstChildrenByType<Element> { it.tagName == "Definitions" }
                            ?.firstChildrenByType<Element> { it.tagName == "CubeBlocks" }
                            ?.eachChildren {
                                (it as? Element)?.takeIf { it.tagName == "Definition" }?.also {
                                    val block = blocks[it] ?: Block.fromXML(it).apply { blocks[it] = this }
                                    val blockId = block.blockId?.let { it.blockSubtypeId.takeUnless { it.isNullOrBlank() } ?: it.blockId } ?: ""
                                    if(blockId.isNotBlank() && targetBlocksMap.containsKey(blockId)){
                                        if(editorController.getBlock() == block){
                                            needUpdateEditor = true
                                        }
                                        targetBlocksMap.getValue(blockId).companents.entries.forEach {
                                            val newValue = it.value.value
                                            if(newValue != null) {
                                                val editor = editors[it.key]
                                                if (editor != null) {
                                                    when (editor.getInputType()) {
                                                        Boolean::class.java -> {
                                                            (editor as EditorController.ABlockInputController<*, Boolean>).applyExtBlockChange(block, (newValue == "true" || newValue == "1" || newValue == "ИСТИНА"))
                                                        }
                                                        String::class.java -> {
                                                            (editor as EditorController.ABlockInputController<*, String>).applyExtBlockChange(block, newValue.takeIf { it != STR_HARD_NULL } ?: "")
                                                        }
                                                        Color::class.java -> {
                                                            (editor as EditorController.ABlockInputController<*, Color>).applyExtBlockChange(block, Color.valueOf(newValue.takeIf { it != STR_HARD_NULL } ?: EditorController.emptyColor.toString()))
                                                        }
                                                        else -> {
                                                            println("undefined type ${editor.getInputType()} with ${editor.name}")
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                    if(needUpdateEditor){
                        editorController.blockExternalChanged()
                    }
                    stage.hide()
                }
                stage.title = "SE Block Editor - change blocks components"
                stage.scene = Scene(root, 950.0, 600.0)
                stage.showAndWait()
            }
            GET_ITEMS_VALUES -> {
                val output = ByteArrayOutputStream()
                CSVWriter(output.bufferedWriter(), arrayOf("key", "input type"), '\t').use {
                    it.startTable(0, null, arrayOf())
                    editorController.inputControllers.forEach { controller ->
                        it.putRow(0, mapOf("key" to controller.name, "input type" to controller.getInputType().simpleName))
                    }
                    it.flush()
                    Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to output.toString(Charsets.UTF_8.name())))
                }
            }
        }
    }

    fun importFromString(imp: String){
        parseXML(imp).run {
            /*val newBlocks = getElementsByTagName("Definition").let {
                (0 until it.length).map { idx ->
                    it.item(idx)
                }
            }.map {
                Block.fromXML(it as Element)
            }
            blocks.clear()
            blocks.addAll(newBlocks)
            pagination.pageCount = blocks.size
            pagination.currentPageIndex = min(pagination.pageCount, pagination.currentPageIndex)
            if(editors.size > pagination.currentPageIndex){//????
                editors[pagination.currentPageIndex]?.first?.changeBlock(blocks[pagination.currentPageIndex])
            }*/
            document = this
            blocks.clear()
            XmlTree.root = document.firstChildren{
                it is Element
            }?.let { createElementItem(it as Element) }
            XmlTree.selectionModel.select((XmlTree.root as ElementItem).findItemRecursive { it.type == TYPE_DEFINITION })
        }
    }

    fun exportToString(): String{
        /*return emptyDocument().run {
            value("CubeBlocks").also { root ->
                blocks.map { block ->
                    value("Definition").let {
                        Block.toXML(block, it, this)
                    }.appendTo(root)
                }
            }
        }.let {
            it.write2()
        }*/
        return document.run {
            blocks.forEach { e, b ->
                e.replace(Block.toXML(b, element("Definition"), this), false)
            }
            blocks.clear()
            XmlTree.selectionModel.clearSelection()
            write2()
        }
    }

    private fun Element.replace(new: Element, notifyBlocks: Boolean = true){
        this.parentNode.replaceChild(new, this)
        if(notifyBlocks){
            blocks[this]?.let {
                blocks[new] = it
                blocks.remove(this)
            }
        }
        (XmlTree.root as? ElementItem)?.let {
            it.findItemRecursive { it.value == this }?.value = new
        }
    }

    inner class ElementItem(
            value: Element,
            val type: Int = TYPE_SIMPLE,
            var childrenInited: Boolean = false
    ): TreeItem<Element>(value){
        fun findItemRecursive(predicate: (ElementItem) -> Boolean): ElementItem?{
            if(predicate(this)){
                return this
            }
            for (child in children) {
                (child as? ElementItem)?.findItemRecursive(predicate)?.let {
                    return it
                }
            }
            return null
        }

        override fun getChildren(): ObservableList<TreeItem<Element>> {
            initChildren()
            return super.getChildren()
        }

        override fun isLeaf(): Boolean {
            initChildren()
            return super.isLeaf()
        }

        private fun initChildren(){
            if(!childrenInited){
                childrenInited = true
                val observable = super.getChildren()
                value.eachChildren {
                    (it as? Element)?.let {
                        observable.add(createElementItem(it))
                    }
                }
            }
        }

    }

    fun createElementItem(element: Element): ElementItem{
        return when(element.tagName){
            "Definition" -> ElementItem(element, TYPE_DEFINITION, true)
            "CubeBlocks" -> ElementItem(element, TYPE_DEFINITION_PARENT)
            else -> ElementItem(element)
        }
    }

    fun createDefinitionMenu(cell: TreeCell<Element>) = ContextMenuBuilder.create()
            .items(
                    MenuItemBuilder.create()
                            .text("Delete")
                            .onAction {
                                getMenuTargetElements(cell.getElementItem()).filter {
                                    it.type == TYPE_DEFINITION
                                }.forEach { element ->
                                    element.parent.children.remove(element)
                                    element.value.let {
                                        it.parentNode.removeChild(it)
                                    }
                                }
                            }.build(),
                    MenuItemBuilder.create()
                            .text("Copy")
                            .onAction {
                                getMenuTargetElements(cell.getElementItem()).filter {
                                    it.type == TYPE_DEFINITION
                                }.map { element ->
                                    val e = element.value
                                    var el = e
                                    blocks[e]?.let {
                                        Block.toXML(it, document.element("Definition"), document).apply {
                                            e.replace(this, false)
                                            el = this
                                            blocks[el] = it
                                            element.value = el
                                        }
                                    }
                                    el
                                }.joinToString(separator = "", prefix = "<?xml version=\"1.0\"?>\r\n<CubeBlocks>\r\n", postfix = "</CubeBlocks>") {
                                    it.write2().removePrefix("<?xml version=\"1.0\"?>").removePrefix("\r").removePrefix("\n")
                                }.let {
                                    Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to it))
                                }
                            }.build(),
                    MenuItemBuilder.create()
                            .text("Export data")
                            .onAction {
                                val list = getMenuTargetElements(cell.getElementItem()).filter {
                                    it.type == TYPE_DEFINITION
                                }
                                val loader = FXMLLoader(javaClass.getResource("ExportSelectedValuesDialog.fxml"))
                                val root = loader.load<Parent>()
                                val stage = Stage()
                                loader.getController<ExportSelectedValuesDialog>().apply {
                                    initColumns(editorController.inputControllers)
                                    listener = { columns ->
                                        val blocks = list.map {
                                            blocks[it.value] ?: Block.fromXML(it.value).apply { blocks[it.value] = this }
                                        }
                                        val outputStream = ByteArrayOutputStream()
                                        CSVWriter(outputStream.bufferedWriter(), columns.map { it.first }.toTypedArray(), '\t').use {
                                            it.startTable(0, null, arrayOf())
                                            for ((i, block) in blocks.withIndex()) {
                                                val map = columns.map {
                                                    it.first to (it.second(block) ?: STR_HARD_NULL)
                                                }.toMap()
                                                it.putRow(i, map)
                                            }
                                            it.flush()
                                            Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to outputStream.toString()))
                                        }
                                        stage.hide()
                                    }
                                }
                                stage.title = "SE Block Editor - change blocks components"
                                stage.scene = Scene(root, 356.0, 320.0)
                                stage.showAndWait()
                            }.build()
                    ).build()

    fun createDefinitionParentMenu(cell: TreeCell<Element>) = ContextMenuBuilder.create()
            .items(
                    MenuItemBuilder.create()
                            .text("New block")
                            .onAction {
                                val element = cell.getElementItem()
                                val newElement = element.value.let {
                                    document.element("Definition").appendTo(it)
                                }
                                val newElementItem = createElementItem(newElement)
                                element.children.add(newElementItem)
                                XmlTree.selectionModel.select(newElementItem)
                            }.build(),
                    MenuItemBuilder.create()
                            .text("Paste")
                            .onAction {
                                val element = cell.getElementItem()
                                val string = Clipboard.getSystemClipboard().string.removePrefix("<?xml version=\"1.0\"?>")
                                parseXML("<?xml version=\"1.0\"?>\n<doc>$string</doc>").apply {
                                    (firstChild as Element).children("Definition").forEach {
                                        val newElement = document.copy(it).appendTo(element.value)
                                        element.children.add(createElementItem(newElement))
                                    }
                                }
                            }.build()
            ).build()

    private fun getMenuTargetElements(element: ElementItem): List<ElementItem> = XmlTree.selectionModel.selectedItems.let {
        if(it.contains(element)){
            it as List<ElementItem>
        } else {
            listOf(element)
        }
    }

    private inline fun TreeCell<Element>.getElementItem() = treeItem as ElementItem

    companion object {
        const val TYPE_SIMPLE = 0
        const val TYPE_DEFINITION = 1
        const val TYPE_DEFINITION_PARENT = 2

        const val STR_HARD_NULL = "NULL"

        val elementConverter = object : StringConverter<Element>() {
            override fun toString(obj: Element?): String? {
                return getElementName(obj)
            }

            override fun fromString(string: String?): Element? {
                return null
            }

        }

        fun getElementName(element: Element?) = element?.let {
            it.tagName + it.firstChildren { it.nodeName == "Id" }.let {
                (it as? Element)?.let {
                    it.child("SubtypeId")?.value()?.takeIf { it.isNotBlank() } ?: it.child("TypeId")?.value()
                }?.let { ": $it" } ?: ""
            }
        } ?: "<nanashi />"
    }
}
