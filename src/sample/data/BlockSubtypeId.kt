package sample.data

import javafx.collections.FXCollections

data class BlockSubtypeId(
        val id: String,
        val name: String
) {

    companion object {
        val global = FXCollections.observableArrayList(
                BlockSubtypeId("Assembler", "Assembler")
        )
    }
}