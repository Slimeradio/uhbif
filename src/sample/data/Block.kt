package sample.data

import helpers.*
import javafx.scene.paint.Color
import org.w3c.dom.Document
import org.w3c.dom.Element
import java.lang.NumberFormatException

class Block() {
    var xsiType: String? = null
    var blockId: BlockId? = null
    var displayName: String? = null
    var description: String? = null
    var raw: String? = null
    var blockPairName: String? = null
    var edgeType: String? = null
    var resourceSourceGroup: String? = null
    var resourceSinkGroup: String? = null
    var thrusterType: String? = null
    var isPublic: Boolean = true//???
    var guiVisible: Boolean = false
    var silenceableByShipSoundSystem: Boolean = false//???
    var blockVariants: MutableList<BlockId>? = null

    var PCU: Int = 1
    var buildTimeSeconds: Long = 10
    var DisassembleRatio: Float? = null
    var DeformationRatio: Float? = null
    var InventoryMaxVolume: Float = 1.0f
    var InventorySize: XYZFloat? = null
    var component: MutableList<ComponentItem>? = null
    var criticalComponent: MPair<String, Int>? = null

    var icon: String? = null
    var model: String? = null
    var buildProgressModels: MutableList<BuildProgressModellItem>? = null
    var cubeSize: String = "Small"
    var isAirTight: Boolean = false
    var size: XYZInt? = null
    var modelOffset: XYZFloat? = null
    var center: XYZInt? = null
    var mirroring: XYZAxis? = null
    var mountPoints: MutableList<MountPointItem>? = null
    var blockTopologyId: String? = null
    var DamageEffectId: Int? = null
    var damageEffectName: String? = null
    var damagedSound: String? = null
    var primarySound: String? = null
    var actionSound: String? = null
    var destroySound: String? = null
    var destroyEffect: String? = null
    var GenerateSound: String? = null
    var IdleSound: String? = null
    var OverlayTexture: String? = null
    var UseModelIntersection: Boolean = false
    var AutorotateMode: String? = null

    var RequiredPowerInput: Float? = null
    var BasePowerInput: Float? = null
    var ConsumptionPower: Float? = null
    var MaxPowerOutput: Float? = null
    var StandbyPowerConsumption: Float? = null
    var OperationalPowerConsumption: Float? = null
    var PowerInput: Float? = null
    var PowerConsumptionIdle: Float? = null
    var PowerConsumptionMoving: Float? = null
    var MaxPowerConsumption: Float? = null
    var MinPowerConsumption: Float? = null
    var MaxStoredPower: Float? = null
    var InitialStoredPowerRatio: Float? = null

    var FlameFullColor: Color? = null
    var FlameIdleColor: Color? = null
    var FlameDamageLengthScale: Float? = null
    var FlameLengthScale: Float? = null
    var NeedsAtmosphereForInfluence: Boolean? = null
    var MinPlanetaryInfluence: Float? = null
    var MaxPlanetaryInfluence: Float? = null
    var EffectivenessAtMinInfluence: Float? = null
    var EffectivenessAtMaxInfluence: Float? = null
    var SlowdownFactor: Float? = null
    var FlamePointMaterial: String? = null
    var FlameLengthMaterial: String? = null
    var FlameFlare: String? = null
    var FlameVisibilityDistance: Int? = null
    var FlameGlareQuerySize: Float? = null
    var ForceMagnitude: Double? = null

    var PowerNeededForJump: Int? = null
    var MaxJumpDistance: Int? = null
    var MaxJumpMass: Int? = null
    var RefineSpeed: Float? = null
    var MaterialEfficiency: Float? = null
    var SensorRadius: Float? = null
    var SensorOffset: Float? = null
    var CutOutRadius: Float? = null
    var CutOutOffset: Float? = null
    var EmissiveColorPreset: String? = null
    var OreAmountPerPullRequest: Int? = null
    var Capacity: Long? = null
    var MaximumRange: Int? = null

    var WeaponDefinitionId: String? = null
    var MinElevationDegrees: Int? = null
    var MaxElevationDegrees: Int? = null
    var MinAzimuthDegrees: Int? = null
    var MaxAzimuthDegrees: Int? = null
    var RotationSpeed: Float? = null
    var ElevationSpeed: Float? = null
    var IdleRotation: Boolean? = null
    var MaxRangeMeters: Int? = null
    var MinFov: Float? = null
    var MaxFov: Float? = null

    var MaxForceMagnitude: Float? = null
    var DangerousTorqueThreshold: Float? = null
    var RotorPart: String? = null
    var PropulsionForce: Int? = null
    var MinHeight: Float? = null
    var MaxHeight: Float? = null
    var SafetyDetach: Int? = null
    var SafetyDetachMax: Int? = null
    var AxleFriction: Int? = null
    var SteeringSpeed: Float? = null
    var AirShockMinSpeed: Int? = null
    var AirShockMaxSpeed: Int? = null
    var AirShockActivationDelay: Int? = null

    companion object {

        class BlockId (
            var blockId: String?,
            var blockSubtypeId: String?
        )

        class ComponentItem(
            var amount: Int = 0,
            var component: String?,
            var deconstructId: BlockId? = null
        )

        class BuildProgressModellItem(
            var progress: Float = 0.0f,
            var file: String?
        )

        class XYZInt(
                var x: Int = 0,
                var y: Int = 0,
                var z: Int = 0
        )

        class XYZFloat(
                var x: Float = 0.0f,
                var y: Float = 0.0f,
                var z: Float = 0.0f
        )

        class XYZAxis(
                var x: String = "",//Companion.Axis.X,
                var y: String = "",//Companion.Axis.Y,
                var z: String = ""//Companion.Axis.Z
        )

        class MountPointItem(
                val side: String,
                val startX: Float,
                val startY: Float,
                val endX: Float,
                val endY: Float,
                val default: Boolean = false
        )

        class Sound{
            lateinit var id: String
            lateinit var name: String
        }

        //enum class Axis{X, Y, Z, XHalfX, XHalfY}

        val REGEX_DOCTYPE = Regex("^<\\?xml (\\w+=\"[^\"]*\")\\?>\n?")

        const val TAG_ID = "Id"
        const val TAG_DISPLAY_NAME = "DisplayName"
        const val TAG_DESCRIPTION = "Description"
        const val TAG_BLOCK_PAIR_NAME = "BlockPairName"
        const val TAG_EDGE_TYPE = "EdgeType"
        const val TAG_RESOURCE_SOURCE_GROUP = "ResourceSourceGroup"
        const val TAG_RESOURCE_SINK_GROUP = "ResourceSinkGroup"
        const val TAG_PUBLIC = "Public"
        const val TAG_GUI_VISIBLE = "GuiVisible"
        const val TAG_THRUSTER_TYPE = "ThrusterType"
        const val TAG_SILENCEABLE_BY_SHIP_SOUND_SYSTEM = "SilenceableByShipSoundSystem"
        const val TAG_BLOCK_VARIANTS = "BlockVariants"

        const val TAG_PCU = "PCU"
        const val TAG_BUILD_TIME_SECONDS = "BuildTimeSeconds"
        const val TAG_DISASSEMBLE_RATIO = "DisassembleRatio"
        const val TAG_DEFORMATION_RATIO = "DeformationRatio"
        const val TAG_INVENTORY_MAX_VOLUME = "InventoryMaxVolume"
        const val TAG_INVENTORY_SIZE = "InventorySize"
        const val TAG_COMPONENTS = "Components"
        const val TAG_CRITICAL_COMPONENT = "CriticalComponent"

        const val TAG_ICON = "Icon"
        const val TAG_MODEL = "Model"
        const val TAG_BUILD_PROGRESS_MODELS = "BuildProgressModels"
        const val TAG_CUBE_SIZE = "CubeSize"
        const val TAG_IS_AIR_TIGHT = "IsAirTight"
        const val TAG_SIZE = "Size"
        const val TAG_MODEL_OFFSET = "ModelOffset"
        const val TAG_CENTER = "Center"
        const val TAG_MIRRORING_X = "MirroringX"
        const val TAG_MIRRORING_Y = "MirroringY"
        const val TAG_MIRRORING_Z = "MirroringZ"
        const val TAG_MOUNT_POINTS = "MountPoints"
        const val TAG_BLOCK_TOPOLOGY = "BlockTopology"
        const val TAG_DAMAGE_EFFECT_ID = "DamageEffectId"
        const val TAG_DAMAGE_EFFECT_NAME = "DamageEffectName"
        const val TAG_DAMAGED_SOUND = "DamagedSound"
        const val TAG_PRIMARY_SOUND = "PrimarySound"
        const val TAG_ACTION_SOUND = "ActionSound"
        const val TAG_DESTROY_SOUND = "DestroySound"
        const val TAG_DESTROY_EFFECT = "DestroyEffect"
        const val TAG_GENERATE_SOUND = "GenerateSound"
        const val TAG_IDLE_SOUND = "IdleSound"
        const val TAG_OVERLAY_TEXTURE = "OverlayTexture"
        const val TAG_USE_MODEL_INTERSECTION = "UseModelIntersection"
        const val TAG_AUTOROTATE_MODE = "AutorotateMode"

        const val TAG_REQUIRED_POWER_INPUT = "RequiredPowerInput"
        const val TAG_BASE_POWER_INPUT = "BasePowerInput"
        const val TAG_CONSUMPTION_POWER = "ConsumptionPower"
        const val TAG_MAX_POWER_OUTPUT = "MaxPowerOutput"
        const val TAG_STAND_BY_POWER_CONSUMPTION = "StandbyPowerConsumption"
        const val TAG_OPERATIONAL_POWER_CONSUMPTION = "OperationalPowerConsumption"
        const val TAG_POWER_INPUT = "PowerInput"
        const val TAG_POWER_CONSUMPTION_IDLE = "PowerConsumptionIdle"
        const val TAG_POWER_CONSUMPTION_MOVING = "PowerConsumptionMoving"
        const val TAG_MAX_POWER_CONSUMPTION = "MaxPowerConsumption"
        const val TAG_MIN_POWER_CONSUMPTION = "MinPowerConsumption"
        const val TAG_MAX_STORED_POWER = "MaxStoredPower"
        const val TAG_INITIAL_STORED_POWER_RATIO = "InitialStoredPowerRatio"

        const val TAG_FLAME_FULL_COLOR = "FlameFullColor"
        const val TAG_FLAME_IDLE_COLOR = "FlameIdleColor"
        const val TAG_FLAME_DAMAGE_LENGTH_SCALE = "FlameDamageLengthScale"
        const val TAG_FLAME_LENGTH_SCALE = "FlameLengthScale"
        const val TAG_NEEDS_ATMOSPHERE_FOR_INFLUENCE = "NeedsAtmosphereForInfluence"
        const val TAG_MIN_PLANETARY_INFLUENCE = "MinPlanetaryInfluence"
        const val TAG_MAX_PLANETARY_INFLUENCE = "MaxPlanetaryInfluence"
        const val TAG_EFFECTIVENESS_AT_MIN_INFLUENCE = "EffectivenessAtMinInfluence"
        const val TAG_EFFECTIVENESS_AT_MAX_INFLUENCE = "EffectivenessAtMaxInfluence"
        const val TAG_SLOWDOWN_FACTOR = "SlowdownFactor"
        const val TAG_FLAME_POINT_MATERIAL = "FlamePointMaterial"
        const val TAG_FLAME_LENGTH_MATERIAL = "FlameLengthMaterial"
        const val TAG_FLAME_FLARE = "FlameFlare"
        const val TAG_FLAME_VISIBILITY_DISTANCE = "FlameVisibilityDistance"
        const val TAG_FLAME_GLARE_QUERY_SIZE = "FlameGlareQuerySize"
        const val TAG_FORCE_MAGNITUDE = "ForceMagnitude"

        const val TAG_POWER_NEEDED_FOR_JUMP = "PowerNeededForJump"
        const val TAG_MAX_JUMP_DISTANCE = "MaxJumpDistance"
        const val TAG_MAX_JUMP_MASS = "MaxJumpMass"
        const val TAG_REFINE_SPEED = "RefineSpeed"
        const val TAG_MATERIAL_EFFICIENCY = "MaterialEfficiency"
        const val TAG_SENSOR_RADIUS = "SensorRadius"
        const val TAG_SENSOR_OFFSET = "SensorOffset"
        const val TAG_CUT_OUT_RADIUS = "CutOutRadius"
        const val TAG_CUT_OUT_OFFSET = "CutOutOffset"
        const val TAG_EMISSIVE_COLOR_PRESET = "EmissiveColorPreset"
        const val TAG_ORE_AMOUNT_PER_PULL_REQUEST = "OreAmountPerPullRequest"
        const val TAG_CAPACITY = "Capacity"
        const val TAG_MAXIMUM_RANGE = "MaximumRange"

        const val TAG_WEAPON_DEFINITION_ID = "WeaponDefinitionId"
        const val TAG_MIN_ELEVATION_DEGREES = "MinElevationDegrees"
        const val TAG_MAX_ELEVATION_DEGREES = "MaxElevationDegrees"
        const val TAG_MIN_AZIMUTH_DEGREES = "MinAzimuthDegrees"
        const val TAG_MAX_AZIMUTH_DEGREES = "MaxAzimuthDegrees"
        const val TAG_ROTATION_SPEED = "RotationSpeed"
        const val TAG_ELEVATION_SPEED = "ElevationSpeed"
        const val TAG_IDLE_ROTATION = "IdleRotation"
        const val TAG_MAX_RANGE_METERS = "MaxRangeMeters"
        const val TAG_MIN_FOV = "MinFov"
        const val TAG_MAX_FOV = "MaxFov"

        const val TAG_MAX_FORCE_MAGNITUDE = "MaxForceMagnitude"
        const val TAG_DANGEROUS_TORQUE_THRESHOLD = "DangerousTorqueThreshold"
        const val TAG_ROTOR_PART = "RotorPart"
        const val TAG_PROPULSION_FORCE = "PropulsionForce"
        const val TAG_MIN_HEIGHT = "MinHeight"
        const val TAG_MAX_HEIGHT = "MaxHeight"
        const val TAG_SAFETY_DETACH = "SafetyDetach"
        const val TAG_SAFETY_DETACHMAX = "SafetyDetachMax"
        const val TAG_AXLE_FRICTION = "AxleFriction"
        const val TAG_STEERING_SPEED = "SteeringSpeed"
        const val TAG_AIR_SHOCK_MIN_SPEED = "AirShockMinSpeed"
        const val TAG_AIR_SHOCK_MAX_SPEED = "AirShockMaxSpeed"
        const val TAG_AIR_SHOCK_ACTIVATION_DELAY = "AirShockActivationDelay"

        val EXISTING_TAGS = setOf(
                TAG_ID,
                TAG_DISPLAY_NAME,
                TAG_DESCRIPTION,
                TAG_BLOCK_PAIR_NAME,
                TAG_EDGE_TYPE,
                TAG_RESOURCE_SOURCE_GROUP,
                TAG_RESOURCE_SINK_GROUP,
                TAG_PUBLIC,
                TAG_GUI_VISIBLE,
                TAG_THRUSTER_TYPE,
                TAG_SILENCEABLE_BY_SHIP_SOUND_SYSTEM,
                TAG_BLOCK_VARIANTS,

                TAG_PCU,
                TAG_BUILD_TIME_SECONDS,
                TAG_DISASSEMBLE_RATIO,
                TAG_DEFORMATION_RATIO,
                TAG_INVENTORY_MAX_VOLUME,
                TAG_INVENTORY_SIZE,
                TAG_COMPONENTS,
                TAG_CRITICAL_COMPONENT,

                TAG_ICON,
                TAG_MODEL,
                TAG_BUILD_PROGRESS_MODELS,
                TAG_CUBE_SIZE,
                TAG_IS_AIR_TIGHT,
                TAG_SIZE,
                TAG_MODEL_OFFSET,
                TAG_CENTER,
                TAG_MIRRORING_X,
                TAG_MIRRORING_Y,
                TAG_MIRRORING_Z,
                TAG_MOUNT_POINTS,
                TAG_BLOCK_TOPOLOGY,
                TAG_DAMAGE_EFFECT_ID,
                TAG_DAMAGE_EFFECT_NAME,
                TAG_DAMAGED_SOUND,
                TAG_PRIMARY_SOUND,
                TAG_ACTION_SOUND,
                TAG_DESTROY_SOUND,
                TAG_DESTROY_EFFECT,
                TAG_GENERATE_SOUND,
                TAG_IDLE_SOUND,
                TAG_OVERLAY_TEXTURE,
                TAG_USE_MODEL_INTERSECTION,
                TAG_AUTOROTATE_MODE,

                TAG_REQUIRED_POWER_INPUT,
                TAG_BASE_POWER_INPUT,
                TAG_CONSUMPTION_POWER,
                TAG_MAX_POWER_OUTPUT,
                TAG_STAND_BY_POWER_CONSUMPTION,
                TAG_OPERATIONAL_POWER_CONSUMPTION,
                TAG_POWER_INPUT,
                TAG_POWER_CONSUMPTION_IDLE,
                TAG_POWER_CONSUMPTION_MOVING,
                TAG_MAX_POWER_CONSUMPTION,
                TAG_MIN_POWER_CONSUMPTION,
                TAG_MAX_STORED_POWER,
                TAG_INITIAL_STORED_POWER_RATIO,

                TAG_FLAME_FULL_COLOR,
                TAG_FLAME_IDLE_COLOR,
                TAG_FLAME_DAMAGE_LENGTH_SCALE,
                TAG_FLAME_LENGTH_SCALE,
                TAG_NEEDS_ATMOSPHERE_FOR_INFLUENCE,
                TAG_MIN_PLANETARY_INFLUENCE,
                TAG_MAX_PLANETARY_INFLUENCE,
                TAG_EFFECTIVENESS_AT_MIN_INFLUENCE,
                TAG_EFFECTIVENESS_AT_MAX_INFLUENCE,
                TAG_SLOWDOWN_FACTOR,
                TAG_FLAME_POINT_MATERIAL,
                TAG_FLAME_LENGTH_MATERIAL,
                TAG_FLAME_FLARE,
                TAG_FLAME_VISIBILITY_DISTANCE,
                TAG_FLAME_GLARE_QUERY_SIZE,
                TAG_FORCE_MAGNITUDE,

                TAG_POWER_NEEDED_FOR_JUMP,
                TAG_MAX_JUMP_DISTANCE,
                TAG_MAX_JUMP_MASS,
                TAG_REFINE_SPEED,
                TAG_MATERIAL_EFFICIENCY,
                TAG_SENSOR_RADIUS,
                TAG_SENSOR_OFFSET,
                TAG_CUT_OUT_RADIUS,
                TAG_CUT_OUT_OFFSET,
                TAG_EMISSIVE_COLOR_PRESET,
                TAG_ORE_AMOUNT_PER_PULL_REQUEST,
                TAG_CAPACITY,
                TAG_MAXIMUM_RANGE,

                TAG_WEAPON_DEFINITION_ID,
                TAG_MIN_ELEVATION_DEGREES,
                TAG_MAX_ELEVATION_DEGREES,
                TAG_MIN_AZIMUTH_DEGREES,
                TAG_MAX_AZIMUTH_DEGREES,
                TAG_ROTATION_SPEED,
                TAG_ELEVATION_SPEED,
                TAG_IDLE_ROTATION,
                TAG_MAX_RANGE_METERS,
                TAG_MIN_FOV,
                TAG_MAX_FOV,

                TAG_MAX_FORCE_MAGNITUDE,
                TAG_DANGEROUS_TORQUE_THRESHOLD,
                TAG_ROTOR_PART,
                TAG_PROPULSION_FORCE,
                TAG_MIN_HEIGHT,
                TAG_MAX_HEIGHT,
                TAG_SAFETY_DETACH,
                TAG_SAFETY_DETACHMAX,
                TAG_AXLE_FRICTION,
                TAG_STEERING_SPEED,
                TAG_AIR_SHOCK_MIN_SPEED,
                TAG_AIR_SHOCK_MAX_SPEED,
                TAG_AIR_SHOCK_ACTIVATION_DELAY)

        fun fromXML(element: Element): Block{
            val ret = Block()
            ret.xsiType = element.attr("xsi:type") {it.removePrefix("MyObjectBuilder_")}
            if(!element.hasChildNodes()){
                System.out.println("empty block!!!")
                return ret
            }
            ret.blockId = element.child(TAG_ID)?.let { BlockId(it.child("TypeId")?.value(), it.child("SubtypeId")?.value()) }
            ret.displayName = element.child(TAG_DISPLAY_NAME)?.value()
            ret.description = element.child(TAG_DESCRIPTION)?.value()
            ret.blockPairName = element.child(TAG_BLOCK_PAIR_NAME)?.value()
            ret.edgeType = element.child(TAG_EDGE_TYPE)?.value()
            ret.resourceSourceGroup = element.child(TAG_RESOURCE_SOURCE_GROUP)?.value()
            ret.resourceSinkGroup = element.child(TAG_RESOURCE_SINK_GROUP)?.value()
            ret.thrusterType = element.child(TAG_THRUSTER_TYPE)?.value()
            ret.isPublic = element.child(TAG_PUBLIC)?.value { it.toBoolean() } ?: ret.isPublic
            ret.guiVisible = element.child(TAG_GUI_VISIBLE)?.value { it.toBoolean() } ?: ret.guiVisible
            ret.silenceableByShipSoundSystem = element.child(TAG_SILENCEABLE_BY_SHIP_SOUND_SYSTEM)?.value { it.toBoolean() } ?: ret.silenceableByShipSoundSystem
            ret.blockVariants = element.child(TAG_BLOCK_VARIANTS)?.let {
                it.children("BlockVariant").asSequence().mapNotNull {
                    BlockId(it.child("TypeId")?.value(), it.child("SubtypeId")?.value())
                }.toMutableList()
            } ?: arrayListOf()

            ret.PCU = element.child(TAG_PCU)?.value { it.toIntOrNull() } ?: ret.PCU
            ret.buildTimeSeconds = element.child(TAG_BUILD_TIME_SECONDS)?.value { it.toLong() } ?: ret.buildTimeSeconds
            ret.DisassembleRatio = element.child(TAG_DISASSEMBLE_RATIO)?.value { it.toFloatOrNull() }
            ret.DeformationRatio = element.child(TAG_DEFORMATION_RATIO)?.value { it.toFloatOrNull() }
            ret.InventoryMaxVolume = element.child(TAG_INVENTORY_MAX_VOLUME)?.value { it.toFloatOrNull() } ?: ret.InventoryMaxVolume
            ret.InventorySize = element.child(TAG_INVENTORY_SIZE)?.let {
                XYZFloat(
                        x = it.child("X")?.value { it.toFloat() } ?: 0.0f,
                        y = it.child("Y")?.value { it.toFloat() } ?: 0.0f,
                        z = it.child("Z")?.value { it.toFloat() } ?: 0.0f)
            }
            ret.component = element.child(TAG_COMPONENTS)?.let {
                it.children("Component").asSequence().mapNotNull {
                    try{
                        ComponentItem(it.attr("Count") { it.toInt() } ?: 0, it.getAttribute("Subtype"), it.child("DeconstructId")?.let {
                            BlockId(it.child("TypeId")?.value(), it.child("SubtypeId")?.value())
                        })
                    } catch (e: NumberFormatException){//WTF
                        println("cannot parse component ${it.write2()} from ${element.write2()}")
                        e.printStackTrace(System.out)
                        null
                    }
                }.toMutableList()
            } ?: arrayListOf()
            ret.criticalComponent = element.child(TAG_CRITICAL_COMPONENT)?.let {
                MPair(it.attr("Subtype") {it} ?: "", it.attr("Index") {it.toIntOrNull()} ?: 0)
            }

            ret.icon = element.child(TAG_ICON)?.value()
            ret.model = element.child(TAG_MODEL)?.value()
            ret.buildProgressModels = element.child(TAG_BUILD_PROGRESS_MODELS)?.let{
                it.children("Model").asSequence().map {
                    BuildProgressModellItem(it.attr("BuildPercentUpperBound") { it.toFloat() } ?: 0.0f, it.getAttribute("File"))
                }.toMutableList()
            } ?: arrayListOf()
            ret.cubeSize = element.child(TAG_CUBE_SIZE)?.value() ?: "Small"
            ret.isAirTight = element.child(TAG_IS_AIR_TIGHT)?.value { it.toBoolean() } ?: false
            ret.size = element.child(TAG_SIZE)?.let {
                XYZInt(
                        x = it.attr("x") { it.toInt() } ?: it.child("X")?.value { it.toInt() } ?: 1,
                        y = it.attr("y") { it.toInt() } ?: it.child("Y")?.value { it.toInt() } ?: 1,
                        z = it.attr("z") { it.toInt() } ?: it.child("Z")?.value { it.toInt() } ?: 1
                )
            }
            ret.modelOffset = element.child(TAG_MODEL_OFFSET)?.let {
                XYZFloat(
                        x = it.attr("x") { it.toFloat() } ?: it.child("X")?.value { it.toFloat() } ?: 1f,
                        y = it.attr("y") { it.toFloat() } ?: it.child("Y")?.value { it.toFloat() } ?: 1f,
                        z = it.attr("z") { it.toFloat() } ?: it.child("Z")?.value { it.toFloat() } ?: 1f
                )
            }
            ret.center = element.child(TAG_CENTER)?.let {
                XYZInt(
                        x = it.attr("x") { it.toInt() } ?: it.child("X")?.value { it.toInt() } ?: 1,
                        y = it.attr("y") { it.toInt() } ?: it.child("Y")?.value { it.toInt() } ?: 1,
                        z = it.attr("z") { it.toInt() } ?: it.child("Z")?.value { it.toInt() } ?: 1
                )
            }
            ret.mirroring = XYZAxis(
                    x = element.child(TAG_MIRRORING_X)?.value() ?: "",// { Axis.valueOf(it.toUpperCase()) } ?: Companion.Axis.X,
                    y = element.child(TAG_MIRRORING_Y)?.value() ?: "",// { Axis.valueOf(it.toUpperCase()) } ?: Companion.Axis.Y,
                    z = element.child(TAG_MIRRORING_Z)?.value() ?: ""// { Axis.valueOf(it.toUpperCase()) } ?: Companion.Axis.Z
            )
            ret.mountPoints = element.child(TAG_MOUNT_POINTS)?.let {
                it.children("MountPoint").asSequence().map {
                    MountPointItem(
                            side = it.getAttribute("Side"),
                            startX = it.attr("StartX") { it.toFloat() } ?: 0.0f,
                            endX = it.attr("EndX") { it.toFloat() } ?: 0.0f,
                            startY = it.attr("StartY") { it.toFloat() } ?: 0.0f,
                            endY = it.attr("EndY") { it.toFloat() } ?: 0.0f,
                            default = it.attr("Default") { it.toBoolean() } ?: false)
                }.toMutableList()
            } ?: arrayListOf()
            ret.blockTopologyId = element.child(TAG_BLOCK_TOPOLOGY)?.value()
            ret.DamageEffectId = element.child(TAG_DAMAGE_EFFECT_ID)?.value{it.toIntOrNull()}
            ret.damageEffectName = element.child(TAG_DAMAGE_EFFECT_NAME)?.value()
            ret.damagedSound = element.child(TAG_DAMAGED_SOUND)?.value()
            ret.primarySound = element.child(TAG_PRIMARY_SOUND)?.value()
            ret.actionSound = element.child(TAG_ACTION_SOUND)?.value()
            ret.destroySound = element.child(TAG_DESTROY_SOUND)?.value()
            ret.destroyEffect = element.child(TAG_DESTROY_EFFECT)?.value()
            ret.GenerateSound = element.child(TAG_GENERATE_SOUND)?.value()
            ret.IdleSound = element.child(TAG_IDLE_SOUND)?.value()
            ret.OverlayTexture = element.child(TAG_OVERLAY_TEXTURE)?.value()
            ret.UseModelIntersection = element.child(TAG_USE_MODEL_INTERSECTION)?.value{it.toBoolean()} ?: ret.UseModelIntersection
            ret.AutorotateMode = element.child(TAG_AUTOROTATE_MODE)?.value()

            ret.RequiredPowerInput = element.child(TAG_REQUIRED_POWER_INPUT)?.value { it.toFloat() } ?: ret.RequiredPowerInput
            ret.BasePowerInput = element.child(TAG_BASE_POWER_INPUT)?.value { it.toFloat() } ?: ret.BasePowerInput
            ret.ConsumptionPower = element.child(TAG_CONSUMPTION_POWER)?.value { it.toFloat() } ?: ret.ConsumptionPower
            ret.MaxPowerOutput = element.child(TAG_MAX_POWER_OUTPUT)?.value { it.toFloat() } ?: ret.MaxPowerOutput
            ret.StandbyPowerConsumption = element.child(TAG_STAND_BY_POWER_CONSUMPTION)?.value { it.toFloat() } ?: ret.StandbyPowerConsumption
            ret.OperationalPowerConsumption = element.child(TAG_OPERATIONAL_POWER_CONSUMPTION)?.value { it.toFloat() } ?: ret.OperationalPowerConsumption
            ret.PowerInput = element.child(TAG_POWER_INPUT)?.value { it.toFloat() } ?: ret.PowerInput
            ret.PowerConsumptionIdle = element.child(TAG_POWER_CONSUMPTION_IDLE)?.value { it.toFloat() } ?: ret.PowerConsumptionIdle
            ret.PowerConsumptionMoving = element.child(TAG_POWER_CONSUMPTION_MOVING)?.value { it.toFloat() } ?: ret.PowerConsumptionMoving
            ret.MaxPowerConsumption = element.child(TAG_MAX_POWER_CONSUMPTION)?.value { it.toFloat() } ?: ret.MaxPowerConsumption
            ret.MinPowerConsumption = element.child(TAG_MIN_POWER_CONSUMPTION)?.value { it.toFloat() } ?: ret.MinPowerConsumption
            ret.MaxStoredPower = element.child(TAG_MAX_STORED_POWER)?.value { it.toFloat() } ?: ret.MaxStoredPower
            ret.InitialStoredPowerRatio = element.child(TAG_INITIAL_STORED_POWER_RATIO)?.value { it.toFloat() } ?: ret.InitialStoredPowerRatio

            ret.FlameFullColor = element.child(TAG_FLAME_FULL_COLOR)?.let {
                Color(
                        it.child("X")?.value { it.toDoubleOrNull() } ?: 0.0,
                        it.child("Y")?.value { it.toDoubleOrNull() } ?: 0.0,
                        it.child("Z")?.value { it.toDoubleOrNull() } ?: 0.0,
                        it.child("W")?.value { it.toDoubleOrNull() } ?: 0.0
                )
            }
            ret.FlameIdleColor = element.child(TAG_FLAME_IDLE_COLOR)?.let {
                Color(
                        it.child("X")?.value { it.toDoubleOrNull() } ?: 0.0,
                        it.child("Y")?.value { it.toDoubleOrNull() } ?: 0.0,
                        it.child("Z")?.value { it.toDoubleOrNull() } ?: 0.0,
                        it.child("W")?.value { it.toDoubleOrNull() } ?: 0.0
                )
            }
            ret.FlameDamageLengthScale = element.child(TAG_FLAME_DAMAGE_LENGTH_SCALE)?.value { it.toFloat() } ?: ret.FlameDamageLengthScale
            ret.FlameLengthScale = element.child(TAG_FLAME_LENGTH_SCALE)?.value { it.toFloat() } ?: ret.FlameLengthScale
            ret.NeedsAtmosphereForInfluence = element.child(TAG_NEEDS_ATMOSPHERE_FOR_INFLUENCE)?.value { it.toBoolean() } ?: ret.NeedsAtmosphereForInfluence
            ret.MinPlanetaryInfluence = element.child(TAG_MIN_PLANETARY_INFLUENCE)?.value { it.toFloat() } ?: ret.MinPlanetaryInfluence
            ret.MaxPlanetaryInfluence = element.child(TAG_MAX_PLANETARY_INFLUENCE)?.value { it.toFloat() } ?: ret.MaxPlanetaryInfluence
            ret.EffectivenessAtMinInfluence = element.child(TAG_EFFECTIVENESS_AT_MIN_INFLUENCE)?.value { it.toFloat() } ?: ret.EffectivenessAtMinInfluence
            ret.EffectivenessAtMaxInfluence = element.child(TAG_EFFECTIVENESS_AT_MAX_INFLUENCE)?.value { it.toFloat() } ?: ret.EffectivenessAtMaxInfluence
            ret.SlowdownFactor = element.child(TAG_SLOWDOWN_FACTOR)?.value { it.toFloat() } ?: ret.SlowdownFactor
            ret.FlamePointMaterial = element.child(TAG_FLAME_POINT_MATERIAL)?.value() ?: ret.FlamePointMaterial
            ret.FlameLengthMaterial = element.child(TAG_FLAME_LENGTH_MATERIAL)?.value() ?: ret.FlameLengthMaterial
            ret.FlameFlare = element.child(TAG_FLAME_FLARE)?.value() ?: ret.FlameFlare
            ret.FlameVisibilityDistance = element.child(TAG_FLAME_VISIBILITY_DISTANCE)?.value { it.toInt() } ?: ret.FlameVisibilityDistance
            ret.FlameGlareQuerySize = element.child(TAG_FLAME_GLARE_QUERY_SIZE)?.value { it.toFloat() } ?: ret.FlameGlareQuerySize
            ret.ForceMagnitude = element.child(TAG_FORCE_MAGNITUDE)?.value { it.toDouble() } ?: ret.ForceMagnitude

            ret.PowerNeededForJump = element.child(TAG_POWER_NEEDED_FOR_JUMP)?.value { it.toInt() } ?: ret.PowerNeededForJump
            ret.MaxJumpDistance = element.child(TAG_MAX_JUMP_DISTANCE)?.value { it.toInt() } ?: ret.MaxJumpDistance
            ret.MaxJumpMass = element.child(TAG_MAX_JUMP_MASS)?.value { it.toInt() } ?: ret.MaxJumpMass
            ret.RefineSpeed = element.child(TAG_REFINE_SPEED)?.value { it.toFloat() } ?: ret.RefineSpeed
            ret.MaterialEfficiency = element.child(TAG_MATERIAL_EFFICIENCY)?.value { it.toFloat() } ?: ret.MaterialEfficiency
            ret.SensorRadius = element.child(TAG_SENSOR_RADIUS)?.value { it.toFloat() } ?: ret.SensorRadius
            ret.SensorOffset = element.child(TAG_SENSOR_OFFSET)?.value { it.toFloat() } ?: ret.SensorOffset
            ret.CutOutRadius = element.child(TAG_CUT_OUT_RADIUS)?.value { it.toFloat() } ?: ret.CutOutRadius
            ret.CutOutOffset = element.child(TAG_CUT_OUT_OFFSET)?.value { it.toFloat() } ?: ret.CutOutOffset
            ret.EmissiveColorPreset = element.child(TAG_EMISSIVE_COLOR_PRESET)?.value() ?: ret.EmissiveColorPreset
            ret.OreAmountPerPullRequest = element.child(TAG_ORE_AMOUNT_PER_PULL_REQUEST)?.value { it.toInt() } ?: ret.OreAmountPerPullRequest
            ret.Capacity = element.child(TAG_CAPACITY)?.value { it.toLong() } ?: ret.Capacity
            ret.MaximumRange = element.child(TAG_MAXIMUM_RANGE)?.value { it.toInt() } ?: ret.MaximumRange

            ret.WeaponDefinitionId = element.child(TAG_WEAPON_DEFINITION_ID)?.attr("Subtype") { it } ?: ret.WeaponDefinitionId
            ret.MinElevationDegrees = element.child(TAG_MIN_ELEVATION_DEGREES)?.value { it.toInt() } ?: ret.MinElevationDegrees
            ret.MaxElevationDegrees = element.child(TAG_MAX_ELEVATION_DEGREES)?.value { it.toInt() } ?: ret.MaxElevationDegrees
            ret.MinAzimuthDegrees = element.child(TAG_MIN_AZIMUTH_DEGREES)?.value { it.toInt() } ?: ret.MinAzimuthDegrees
            ret.MaxAzimuthDegrees = element.child(TAG_MAX_AZIMUTH_DEGREES)?.value { it.toInt() } ?: ret.MaxAzimuthDegrees
            ret.RotationSpeed = element.child(TAG_ROTATION_SPEED)?.value { it.toFloat() } ?: ret.RotationSpeed
            ret.ElevationSpeed = element.child(TAG_ELEVATION_SPEED)?.value { it.toFloat() } ?: ret.ElevationSpeed
            ret.IdleRotation = element.child(TAG_IDLE_ROTATION)?.value { it.toBoolean() } ?: ret.IdleRotation
            ret.MaxRangeMeters = element.child(TAG_MAX_RANGE_METERS)?.value { it.toInt() } ?: ret.MaxRangeMeters
            ret.MinFov = element.child(TAG_MIN_FOV)?.value { it.toFloat() } ?: ret.MinFov
            ret.MaxFov = element.child(TAG_MAX_FOV)?.value { it.toFloat() } ?: ret.MaxFov

            ret.MaxForceMagnitude = element.child(TAG_MAX_FORCE_MAGNITUDE)?.value { it.toFloat() } ?: ret.MaxForceMagnitude
            ret.DangerousTorqueThreshold = element.child(TAG_DANGEROUS_TORQUE_THRESHOLD)?.value { it.toFloat() } ?: ret.DangerousTorqueThreshold
            ret.RotorPart = element.child(TAG_ROTOR_PART)?.value () ?: ret.RotorPart
            ret.PropulsionForce = element.child(TAG_PROPULSION_FORCE)?.value { it.toInt() } ?: ret.PropulsionForce
            ret.MinHeight = element.child(TAG_MIN_HEIGHT)?.value { it.toFloat() } ?: ret.MinHeight
            ret.MaxHeight = element.child(TAG_MAX_HEIGHT)?.value { it.toFloat() } ?: ret.MaxHeight
            ret.SafetyDetach = element.child(TAG_SAFETY_DETACH)?.value { it.toInt() } ?: ret.SafetyDetach
            ret.SafetyDetachMax = element.child(TAG_SAFETY_DETACHMAX)?.value { it.toInt() } ?: ret.SafetyDetachMax
            ret.AxleFriction = element.child(TAG_AXLE_FRICTION)?.value { it.toInt() } ?: ret.AxleFriction
            ret.SteeringSpeed = element.child(TAG_STEERING_SPEED)?.value { it.toFloat() } ?: ret.SteeringSpeed
            ret.AirShockMinSpeed = element.child(TAG_AIR_SHOCK_MIN_SPEED)?.value { it.toInt() } ?: ret.AirShockMinSpeed
            ret.AirShockMaxSpeed = element.child(TAG_AIR_SHOCK_MAX_SPEED)?.value { it.toInt() } ?: ret.AirShockMaxSpeed
            ret.AirShockActivationDelay = element.child(TAG_AIR_SHOCK_ACTIVATION_DELAY)?.value { it.toInt() } ?: ret.AirShockActivationDelay
            element.eachChildren {
                if(it is Element && !EXISTING_TAGS.contains(it.tagName)){
                    ret.raw = (ret.raw ?: "") + it.write2().replace(REGEX_DOCTYPE, "")
                }
            }
            return ret
        }

        fun toXML(block: Block, root: Element, doc: Document): Element{
            block.xsiType?.takeIf { it.isNotEmpty() }?.apply {
                root.attr("xsi:type", this) { "MyObjectBuilder_$it" }
            }
            block.blockId?.also {
                doc.element(TAG_ID).addChildren {
                    arrayListOf(
                            element("TypeId").value(it.blockId),
                            element("SubtypeId").value(it.blockSubtypeId)
                    )
                }.appendTo(root)
            }
            block.displayName?.also {
                doc.element(TAG_DISPLAY_NAME).value(it).appendTo(root)
            }
            block.description?.also { doc.element(TAG_DESCRIPTION).value(it).appendTo(root) }
            block.blockPairName?.also { doc.element(TAG_BLOCK_PAIR_NAME).value(it).appendTo(root) }
            block.edgeType?.also { doc.element(TAG_EDGE_TYPE).value(it).appendTo(root) }
            block.resourceSourceGroup?.also { doc.element(TAG_RESOURCE_SOURCE_GROUP).value(it).appendTo(root) }
            block.resourceSinkGroup?.also { doc.element(TAG_RESOURCE_SINK_GROUP).value(it).appendTo(root) }
            block.thrusterType?.also { doc.element(TAG_THRUSTER_TYPE).value(it).appendTo(root) }
            block.isPublic.also { doc.element(TAG_PUBLIC).value(it).appendTo(root) }
            block.guiVisible.also { doc.element(TAG_GUI_VISIBLE).value (it).appendTo(root) }
            block.silenceableByShipSoundSystem.also { doc.element(TAG_SILENCEABLE_BY_SHIP_SOUND_SYSTEM).value(it).appendTo(root) }
            block.blockVariants.takeUnless { it.isNullOrEmpty() }?.also {
                doc.element(TAG_BLOCK_VARIANTS).addChildren {
                    it.map { item ->
                        doc.element("BlockVariant").addChildren {
                            arrayListOf(
                                    element("TypeId").value(item.blockId),
                                    element("SubtypeId").value(item.blockSubtypeId)
                            )
                        }
                    }
                }.appendTo(root)
            }

            block.PCU.takeIf { it != 1 }?.also { doc.element(TAG_PCU).value(it).appendTo(root) }
            block.buildTimeSeconds.also { doc.element(TAG_BUILD_TIME_SECONDS).value(it).appendTo(root) }
            block.DisassembleRatio?.also { doc.element(TAG_DISASSEMBLE_RATIO).value(it).appendTo(root) }
            block.DeformationRatio?.also { doc.element(TAG_DEFORMATION_RATIO).value(it).appendTo(root) }
            block.InventoryMaxVolume.also { doc.element(TAG_INVENTORY_MAX_VOLUME).value(it).appendTo(root) }
            block.InventorySize?.also {
                doc.element(TAG_INVENTORY_SIZE).addChildren {
                    arrayListOf(
                            element("X").value(it.x),
                            element("Y").value(it.y),
                            element("Z").value(it.z))
                }.appendTo(root)
            }
            block.component?.also {
                doc.element(TAG_COMPONENTS).addChildren {
                    it.map { item ->
                        element("Component").attr("Count", item.amount).attr("Subtype", item.component).apply {
                            item.deconstructId?.let {
                                doc.element("DeconstructId").addChildren {
                                    arrayListOf(
                                            element("TypeId").value(it.blockId),
                                            element("SubtypeId").value(it.blockSubtypeId)
                                    )
                                }.appendTo(this)
                            }
                        }
                    }
                }.appendTo(root)
            }
            block.criticalComponent?.also {
                doc.element(TAG_CRITICAL_COMPONENT).attr("Subtype", it.first).attr("Index", it.second).appendTo(root)
            }

            block.guiVisible.also { doc.element(TAG_GUI_VISIBLE).value (it).appendTo(root) }
            block.icon?.also { doc.element(TAG_ICON).value(it).appendTo(root) }
            block.model?.also { doc.element(TAG_MODEL).value(it).appendTo(root) }
            block.buildProgressModels?.also {
                doc.element(TAG_BUILD_PROGRESS_MODELS).addChildren{
                    it.map { item ->
                        element("Model").attr("BuildPercentUpperBound", item.progress).attr("File", item.file)
                    }
                }.appendTo(root)
            }
            block.cubeSize.also { doc.element(TAG_CUBE_SIZE).value (it).appendTo(root) }
            block.isAirTight.also { doc.element(TAG_IS_AIR_TIGHT).value (it).appendTo(root) }
            block.size?.also {
                doc.element(TAG_SIZE).attr("x", it.x).attr("y", it.y).attr("z", it.z).appendTo(root)
            }
            block.modelOffset?.also {
                doc.element(TAG_MODEL_OFFSET).attr("x", it.x).attr("y", it.y).attr("z", it.z).appendTo(root)
            }
            block.center?.also {
                doc.element(TAG_CENTER).attr("x", it.x).attr("y", it.y).attr("z", it.z).appendTo(root)
            }
            block.mirroring?.also {
                if(it.x.isNotBlank() ){//!= Companion.Axis.X){
                    doc.element(TAG_MIRRORING_X).value(it.x)
                }
                if(it.y.isNotBlank() ){//!= Companion.Axis.Y){
                    doc.element(TAG_MIRRORING_Y).value(it.y)
                }
                if(it.z.isNotBlank() ){//!= Companion.Axis.Z){
                    doc.element(TAG_MIRRORING_Z).value(it.z)
                }
            }
            block.mountPoints?.also {
                doc.element(TAG_MOUNT_POINTS).addChildren{
                    it.map { item ->
                        element("MountPoint")
                                .attr("Side", item.side)
                                .attr("StartX", item.startX)
                                .attr("StartY", item.startY)
                                .attr("EndX", item.endX)
                                .attr("EndY", item.endY)
                                .attr("Default", item.default.takeIf { it })//true or null
                    }
                }.appendTo(root)
            }
            block.blockTopologyId.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_BLOCK_TOPOLOGY).value(it).appendTo(root) }
            block.DamageEffectId?.also { doc.element(TAG_DAMAGE_EFFECT_ID).value(it).appendTo(root) }
            block.damageEffectName.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_DAMAGE_EFFECT_NAME).value(it).appendTo(root) }
            block.damagedSound.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_DAMAGED_SOUND).value(it).appendTo(root) }
            block.primarySound.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_PRIMARY_SOUND).value(it).appendTo(root) }
            block.actionSound.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_ACTION_SOUND).value(it).appendTo(root) }
            block.destroySound.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_DESTROY_SOUND).value(it).appendTo(root) }
            block.destroyEffect.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_DESTROY_EFFECT).value(it).appendTo(root) }
            block.GenerateSound.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_GENERATE_SOUND).value(it).appendTo(root) }
            block.IdleSound.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_IDLE_SOUND).value(it).appendTo(root) }
            block.OverlayTexture.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_OVERLAY_TEXTURE).value(it).appendTo(root) }
            block.UseModelIntersection.also { doc.element(TAG_USE_MODEL_INTERSECTION).value(it).appendTo(root) }
            block.AutorotateMode.takeUnless { it.isNullOrBlank() }?.also { doc.element(TAG_AUTOROTATE_MODE).value(it).appendTo(root) }

            block.RequiredPowerInput?.also { doc.element(TAG_REQUIRED_POWER_INPUT).value(it).appendTo(root) }
            block.BasePowerInput?.also { doc.element(TAG_BASE_POWER_INPUT).value(it).appendTo(root) }
            block.ConsumptionPower?.also { doc.element(TAG_CONSUMPTION_POWER).value(it).appendTo(root) }
            block.MaxPowerOutput?.also { doc.element(TAG_MAX_POWER_OUTPUT).value(it).appendTo(root) }
            block.StandbyPowerConsumption?.also { doc.element(TAG_STAND_BY_POWER_CONSUMPTION).value(it).appendTo(root) }
            block.OperationalPowerConsumption?.also { doc.element(TAG_OPERATIONAL_POWER_CONSUMPTION).value(it).appendTo(root) }
            block.PowerInput?.also { doc.element(TAG_POWER_INPUT).value(it).appendTo(root) }
            block.PowerConsumptionIdle?.also { doc.element(TAG_POWER_CONSUMPTION_IDLE).value(it).appendTo(root) }
            block.PowerConsumptionMoving?.also { doc.element(TAG_POWER_CONSUMPTION_MOVING).value(it).appendTo(root) }
            block.MaxPowerConsumption?.also { doc.element(TAG_MAX_POWER_CONSUMPTION).value(it).appendTo(root) }
            block.MinPowerConsumption?.also { doc.element(TAG_MIN_POWER_CONSUMPTION).value(it).appendTo(root) }
            block.MaxStoredPower?.also { doc.element(TAG_MAX_STORED_POWER).value(it).appendTo(root) }
            block.InitialStoredPowerRatio?.also { doc.element(TAG_INITIAL_STORED_POWER_RATIO).value(it).appendTo(root) }

            block.FlameFullColor?.also {
                doc.element(TAG_FLAME_FULL_COLOR).addChildren {
                    arrayListOf(
                            element("X").value(it.red),
                            element("Y").value(it.green),
                            element("Z").value(it.blue),
                            element("W").value(it.opacity))
                }.appendTo(root)
            }
            block.FlameIdleColor?.also {
                doc.element(TAG_FLAME_IDLE_COLOR).addChildren {
                    arrayListOf(
                            element("X").value(it.red),
                            element("Y").value(it.green),
                            element("Z").value(it.blue),
                            element("W").value(it.opacity))
                }.appendTo(root)
            }
            block.FlameDamageLengthScale?.also { doc.element(TAG_FLAME_DAMAGE_LENGTH_SCALE).value(it).appendTo(root) }
            block.FlameLengthScale?.also { doc.element(TAG_FLAME_LENGTH_SCALE).value(it).appendTo(root) }
            block.NeedsAtmosphereForInfluence?.also { doc.element(TAG_NEEDS_ATMOSPHERE_FOR_INFLUENCE).value(it).appendTo(root) }
            block.MinPlanetaryInfluence?.also { doc.element(TAG_MIN_PLANETARY_INFLUENCE).value(it).appendTo(root) }
            block.MaxPlanetaryInfluence?.also { doc.element(TAG_MAX_PLANETARY_INFLUENCE).value(it).appendTo(root) }
            block.EffectivenessAtMinInfluence?.also { doc.element(TAG_EFFECTIVENESS_AT_MIN_INFLUENCE).value(it).appendTo(root) }
            block.EffectivenessAtMaxInfluence?.also { doc.element(TAG_EFFECTIVENESS_AT_MAX_INFLUENCE).value(it).appendTo(root) }
            block.SlowdownFactor?.also { doc.element(TAG_SLOWDOWN_FACTOR).value(it).appendTo(root) }
            block.FlamePointMaterial?.also { doc.element(TAG_FLAME_POINT_MATERIAL).value(it).appendTo(root) }
            block.FlameLengthMaterial?.also { doc.element(TAG_FLAME_LENGTH_MATERIAL).value(it).appendTo(root) }
            block.FlameFlare?.also { doc.element(TAG_FLAME_FLARE).value(it).appendTo(root) }
            block.FlameVisibilityDistance?.also { doc.element(TAG_FLAME_VISIBILITY_DISTANCE).value(it).appendTo(root) }
            block.FlameGlareQuerySize?.also { doc.element(TAG_FLAME_GLARE_QUERY_SIZE).value(it).appendTo(root) }
            block.ForceMagnitude?.also { doc.element(TAG_FORCE_MAGNITUDE).value(it).appendTo(root) }

            block.PowerNeededForJump?.also { doc.element(TAG_POWER_NEEDED_FOR_JUMP).value(it).appendTo(root) }
            block.MaxJumpDistance?.also { doc.element(TAG_MAX_JUMP_DISTANCE).value(it).appendTo(root) }
            block.MaxJumpMass?.also { doc.element(TAG_MAX_JUMP_MASS).value(it).appendTo(root) }
            block.RefineSpeed?.also { doc.element(TAG_REFINE_SPEED).value(it).appendTo(root) }
            block.MaterialEfficiency?.also { doc.element(TAG_MATERIAL_EFFICIENCY).value(it).appendTo(root) }
            block.SensorRadius?.also { doc.element(TAG_SENSOR_RADIUS).value(it).appendTo(root) }
            block.SensorOffset?.also { doc.element(TAG_SENSOR_OFFSET).value(it).appendTo(root) }
            block.CutOutRadius?.also { doc.element(TAG_CUT_OUT_RADIUS).value(it).appendTo(root) }
            block.CutOutOffset?.also { doc.element(TAG_CUT_OUT_OFFSET).value(it).appendTo(root) }
            block.EmissiveColorPreset?.also { doc.element(TAG_EMISSIVE_COLOR_PRESET).value(it).appendTo(root) }
            block.OreAmountPerPullRequest?.also { doc.element(TAG_ORE_AMOUNT_PER_PULL_REQUEST).value(it).appendTo(root) }
            block.Capacity?.also { doc.element(TAG_CAPACITY).value(it).appendTo(root) }
            block.MaximumRange?.also { doc.element(TAG_MAXIMUM_RANGE).value(it).appendTo(root) }

            block.WeaponDefinitionId?.also { doc.element(TAG_WEAPON_DEFINITION_ID).attr("Subtype", it).appendTo(root) }
            block.MinElevationDegrees?.also { doc.element(TAG_MIN_ELEVATION_DEGREES).value(it).appendTo(root) }
            block.MaxElevationDegrees?.also { doc.element(TAG_MAX_ELEVATION_DEGREES).value(it).appendTo(root) }
            block.MinAzimuthDegrees?.also { doc.element(TAG_MIN_AZIMUTH_DEGREES).value(it).appendTo(root) }
            block.MaxAzimuthDegrees?.also { doc.element(TAG_MAX_AZIMUTH_DEGREES).value(it).appendTo(root) }
            block.RotationSpeed?.also { doc.element(TAG_ROTATION_SPEED).value(it).appendTo(root) }
            block.ElevationSpeed?.also { doc.element(TAG_ELEVATION_SPEED).value(it).appendTo(root) }
            block.IdleRotation?.also { doc.element(TAG_IDLE_ROTATION).value(it).appendTo(root) }
            block.MaxRangeMeters?.also { doc.element(TAG_MAX_RANGE_METERS).value(it).appendTo(root) }
            block.MinFov?.also { doc.element(TAG_MIN_FOV).value(it).appendTo(root) }
            block.MaxFov?.also { doc.element(TAG_MAX_FOV).value(it).appendTo(root) }

            block.MaxForceMagnitude?.also { doc.element(TAG_MAX_FORCE_MAGNITUDE).value(it).appendTo(root) }
            block.DangerousTorqueThreshold?.also { doc.element(TAG_DANGEROUS_TORQUE_THRESHOLD).value(it).appendTo(root) }
            block.RotorPart?.also { doc.element(TAG_ROTOR_PART).value(it).appendTo(root) }
            block.PropulsionForce?.also { doc.element(TAG_PROPULSION_FORCE).value(it).appendTo(root) }
            block.MinHeight?.also { doc.element(TAG_MIN_HEIGHT).value(it).appendTo(root) }
            block.MaxHeight?.also { doc.element(TAG_MAX_HEIGHT).value(it).appendTo(root) }
            block.SafetyDetach?.also { doc.element(TAG_SAFETY_DETACH).value(it).appendTo(root) }
            block.SafetyDetachMax?.also { doc.element(TAG_SAFETY_DETACHMAX).value(it).appendTo(root) }
            block.AxleFriction?.also { doc.element(TAG_AXLE_FRICTION).value(it).appendTo(root) }
            block.SteeringSpeed?.also { doc.element(TAG_STEERING_SPEED).value(it).appendTo(root) }
            block.AirShockMinSpeed?.also { doc.element(TAG_AIR_SHOCK_MIN_SPEED).value(it).appendTo(root) }
            block.AirShockMaxSpeed?.also { doc.element(TAG_AIR_SHOCK_MAX_SPEED).value(it).appendTo(root) }
            block.AirShockActivationDelay?.also { doc.element(TAG_AIR_SHOCK_ACTIVATION_DELAY).value(it).appendTo(root) }
            block.raw.takeUnless {
                it.isNullOrBlank() }?.also { parseXML("<pseudo_root>${it.trimEnd()}</pseudo_root>").firstChild.eachChildren {
                    doc.importNode(it, true).appendTo(root)
                }
            }
            return root
        }
    }
}