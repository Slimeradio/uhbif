package sample.data

import javafx.collections.FXCollections

data class BlockTopologyItem(
        val id: String,
        val name: String
) {

    companion object {
        val global = FXCollections.observableArrayList(
                BlockTopologyItem("Assembler", "Assembler")
        )
    }
}