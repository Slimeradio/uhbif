package sample

import helpers.MPair
import helpers.highlightXML
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.*
import javafx.scene.paint.Color
import org.fxmisc.richtext.CodeArea
import sample.data.Block

import java.net.URL
import java.text.ParseException
import java.util.ResourceBundle

public class EditorController() : Initializable {
    val block: ObjectProperty<Block> = SimpleObjectProperty(this, "block")

    val inputControllers = ArrayList<ABlockInputController<*, *>>()

    //region General
    @FXML
    lateinit var BlId: TextField

    @FXML
    lateinit var BlSubtypeId: TextField

    @FXML
    lateinit var BlDisplayName: TextField

    @FXML
    lateinit var BlDescription: TextArea

    @FXML
    lateinit var BlRAW: CodeArea//TextArea

    @FXML
    lateinit var BlBlockPairName: TextField

    @FXML
    lateinit var BlEdgeType: TextField

    @FXML
    lateinit var BlResourceSourceGroup: TextField

    @FXML
    lateinit var BlResourceSinkGroup: TextField

    @FXML
    lateinit var BlPublic: CheckBox

    @FXML
    lateinit var BlGuiVisible: CheckBox

    @FXML
    lateinit var BlSilenceableByShipSoundSystem: CheckBox

    @FXML
    lateinit var BlThrusterType: TextField

    @FXML
    lateinit var BlXsiType: TextField

    @FXML
    lateinit var BlBlockVariants: TextArea

    //endregion
    //region Balance
    @FXML
    lateinit var BlPCU: TextField

    @FXML
    lateinit var BlBuildTimeSeconds: TextField

    @FXML
    lateinit var BlDisassembleRatio: TextField

    @FXML
    lateinit var BlDeformationRatio: TextField

    @FXML
    lateinit var BlInventoryMaxVolume: TextField

    @FXML
    lateinit var BlInventorySizeX: TextField
    @FXML
    lateinit var BlInventorySizeY: TextField
    @FXML
    lateinit var BlInventorySizeZ: TextField

    @FXML
    lateinit var BlComponent: TextArea

    //endregion
    //region Model
    @FXML
    lateinit var BlIcon: TextField

    @FXML
    lateinit var BlModel: TextField

    @FXML
    lateinit var BlBuildProgressModels: TextArea

    @FXML
    lateinit var BlCubeSize: CheckBox

    @FXML
    lateinit var BlIsAirTight: CheckBox

    @FXML
    lateinit var BlSizeX: TextField
    @FXML
    lateinit var BlSizeY: TextField
    @FXML
    lateinit var BlSizeZ: TextField

    @FXML
    lateinit var BlModelOffsetX: TextField
    @FXML
    lateinit var BlModelOffsetY: TextField
    @FXML
    lateinit var BlModelOffsetZ: TextField

    @FXML
    lateinit var BlCenterX: TextField
    @FXML
    lateinit var BlCenterY: TextField
    @FXML
    lateinit var BlCenterZ: TextField

    @FXML
    lateinit var BlMirroringX: TextField
    @FXML
    lateinit var BlMirroringY: TextField
    @FXML
    lateinit var BlMirroringZ: TextField

    @FXML
    lateinit var BlMountPoints: TextArea

    @FXML
    lateinit var BlBlockTopology: TextField

    @FXML
    lateinit var BlDamageEffectId: TextField

    @FXML
    lateinit var BlDamageEffectName: TextField

    @FXML
    lateinit var BlDamagedSound: TextField

    @FXML
    lateinit var BlPrimarySound: TextField

    @FXML
    lateinit var BlActionSound: TextField

    @FXML
    lateinit var BlDestroyEffect: TextField

    @FXML
    lateinit var BlDestroySound: TextField

    @FXML
    lateinit var BlGenerateSound: TextField

    @FXML
    lateinit var BlIdleSound: TextField

    @FXML
    lateinit var BlOverlayTexture: TextField

    @FXML
    lateinit var BlUseModelIntersection: CheckBox

    @FXML
    lateinit var BlAutorotateMode: TextField
    //endregion
    //region Extra

    @FXML
    lateinit var BlRequiredPowerInput: TextField

    @FXML
    lateinit var BlBasePowerInput: TextField

    @FXML
    lateinit var BlConsumptionPower: TextField

    @FXML
    lateinit var BlMaxPowerOutput: TextField

    @FXML
    lateinit var BlStandByPowerConsumption: TextField

    @FXML
    lateinit var BlOperationalPowerConsumption: TextField

    @FXML
    lateinit var BlPowerInput: TextField

    @FXML
    lateinit var BlPowerConsumptionIdle: TextField

    @FXML
    lateinit var BlPowerConsumptionMoving: TextField

    @FXML
    lateinit var BlMaxPowerConsumption: TextField

    @FXML
    lateinit var BlMinPowerConsumption: TextField

    @FXML
    lateinit var BlMaxStoredPower: TextField

    @FXML
    lateinit var BlInitialStoredPowerRatio: TextField
    //endregion
    //region Thrusters

    @FXML
    lateinit var BlFlameFullColorRGBO: ColorPicker

    @FXML
    lateinit var BlFlameIdleColorRGBO: ColorPicker

    @FXML
    lateinit var BlFlameDamageLengthScale: TextField

    @FXML
    lateinit var BlFlameLengthScale: TextField

    @FXML
    lateinit var BlNeedsAtmosphereForInfluence: CheckBox

    @FXML
    lateinit var BlMinPlanetaryInfluence: TextField

    @FXML
    lateinit var BlMaxPlanetaryInfluence: TextField

    @FXML
    lateinit var BlEffectivenessAtMinInfluence: TextField

    @FXML
    lateinit var BlEffectivenessAtMaxInfluence: TextField

    @FXML
    lateinit var BlSlowdownFactor: TextField

    @FXML
    lateinit var BlFlamePointMaterial: TextField

    @FXML
    lateinit var BlFlameLengthMaterial: TextField

    @FXML
    lateinit var BlFlameFlare: TextField

    @FXML
    lateinit var BlFlameVisibilityDistance: TextField

    @FXML
    lateinit var BlFlameGlareQuerySize: TextField

    @FXML
    lateinit var BlForceMagnitude: TextField
    //endregion
    //region Specials

    @FXML
    lateinit var BlPowerNeededForJump: TextField

    @FXML
    lateinit var BlMaxJumpDistance: TextField

    @FXML
    lateinit var BlMaxJumpMass: TextField

    @FXML
    lateinit var BlRefineSpeed: TextField

    @FXML
    lateinit var BlMaterialEfficiency: TextField

    @FXML
    lateinit var BlSensorRadius: TextField

    @FXML
    lateinit var BlSensorOffset: TextField

    @FXML
    lateinit var BlCutOutRadius: TextField

    @FXML
    lateinit var BlCutOutOffset: TextField

    @FXML
    lateinit var BlEmissiveColorPreset: TextField

    @FXML
    lateinit var BlOreAmountPerPullRequest: TextField

    @FXML
    lateinit var BlCapacity: TextField

    @FXML
    lateinit var BlMaximumRange: TextField
    //endregion
    //region Weap

    @FXML
    lateinit var BlWeaponDefinitionId: TextField

    @FXML
    lateinit var BlMinElevationDegrees: TextField

    @FXML
    lateinit var BlMaxElevationDegrees: TextField

    @FXML
    lateinit var BlMinAzimuthDegrees: TextField

    @FXML
    lateinit var BlMaxAzimuthDegrees: TextField

    @FXML
    lateinit var BlRotationSpeed: TextField

    @FXML
    lateinit var BlElevationSpeed: TextField

    @FXML
    lateinit var BlIdleRotation: CheckBox

    @FXML
    lateinit var BlMaxRangeMeters: TextField

    @FXML
    lateinit var BlMinFov: TextField

    @FXML
    lateinit var BlMaxFov: TextField
    //endregion
    //region Wheels

    @FXML
    lateinit var BlMaxForceMagnitude: TextField

    @FXML
    lateinit var BlDangerousTorqueThreshold: TextField

    @FXML
    lateinit var BlRotorPart: TextField

    @FXML
    lateinit var BlPropulsionForce: TextField

    @FXML
    lateinit var BlMinHeight: TextField

    @FXML
    lateinit var BlMaxHeight: TextField

    @FXML
    lateinit var BlSafetyDetach: TextField

    @FXML
    lateinit var BlSafetyDetachMax: TextField

    @FXML
    lateinit var BlAxleFriction: TextField

    @FXML
    lateinit var BlSteeringSpeed: TextField

    @FXML
    lateinit var BlAirShockMinSpeed: TextField

    @FXML
    lateinit var BlAirShockMaxSpeed: TextField

    @FXML
    lateinit var BlAirShockActivationDelay: TextField
    //endregion

    override fun initialize(location: URL?,  resources: ResourceBundle?) {

        /*BlMirroringX.items = FXCollections.observableArrayList(*Block.Companion.Axis.values())
        BlMirroringY.items = FXCollections.observableArrayList(*Block.Companion.Axis.values())
        BlMirroringZ.items = FXCollections.observableArrayList(*Block.Companion.Axis.values())*/

        BlRAW.highlightXML(Main::class.java)

        //region General accessors
        inputControllers += object : FieldInputController<String>("blockId", BlId, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.blockId?.blockId ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                (block.blockId ?: Block.Companion.BlockId(null, null).apply { block.blockId = this }).blockId = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("blockSubtypeId", BlSubtypeId, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.blockId?.blockSubtypeId ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                (block.blockId ?: Block.Companion.BlockId(null, null).apply { block.blockId = this }).blockSubtypeId = newValue
            }
        }
        /*inputControllers += object : ComboBoxInputController<String, BlockSubtypeId>(BlSubtypeId, block){
            override fun getBlockValue(block: Block): BlockSubtypeId {
                val id = block.blockId?.blockSubtypeId ?: ""
                return findItem(id) ?: BlockSubtypeId(id, "")
            }

            override fun setBlockValue(block: Block, newValue: BlockSubtypeId) {
                (block.blockId ?: Block.Companion.BlockId(null, null).apply { block.blockId = this }).blockSubtypeId = newValue.id
            }

            override fun getId(value: BlockSubtypeId): String = value.id
        }*/

        inputControllers += object : FieldInputController<String>("displayName", BlDisplayName, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.displayName ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.displayName = newValue
            }
        }

        inputControllers += object : TextAreaInputController<String>("description", BlDescription, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.description ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.description = newValue
            }
        }

        inputControllers += object : CodeAreaInputController<String>("raw", BlRAW, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.raw ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.raw = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("blockPairName", BlBlockPairName, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.blockPairName ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.blockPairName = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("edgeType", BlEdgeType, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.edgeType ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.edgeType = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("resourceSourceGroup", BlResourceSourceGroup, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.resourceSourceGroup ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.resourceSourceGroup = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("resourceSinkGroup", BlResourceSinkGroup, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.resourceSinkGroup ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.resourceSinkGroup = newValue
            }
        }

        inputControllers += object : CheckboxInputController("isPublic", BlPublic, block){
            override fun getBlockValue(block: Block): Boolean = block.isPublic

            override fun setBlockValue(block: Block, newValue: Boolean) {
                block.isPublic = newValue
            }
        }

        inputControllers += object : CheckboxInputController("guiVisible", BlGuiVisible, block){
            override fun getBlockValue(block: Block): Boolean = block.guiVisible

            override fun setBlockValue(block: Block, newValue: Boolean) {
                block.guiVisible = newValue
            }
        }

        inputControllers += object : CheckboxInputController("silenceableByShipSoundSystem", BlSilenceableByShipSoundSystem, block){
            override fun getBlockValue(block: Block): Boolean = block.silenceableByShipSoundSystem

            override fun setBlockValue(block: Block, newValue: Boolean) {
                block.silenceableByShipSoundSystem = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("thrusterType", BlThrusterType, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.thrusterType ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.thrusterType = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("xsiType", BlXsiType, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.xsiType ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.xsiType = newValue
            }
        }

        inputControllers += object : TextAreaInputController<MutableList<Block.Companion.BlockId>>(
                "blockVariants",
                BlBlockVariants,
                block,
                object : LinesRegexConverter<Block.Companion.BlockId>(Regex("([a-zA-Z0-9_]*)\\/([a-zA-Z0-9_]*)")){
                    override fun toLine(item: Block.Companion.BlockId): String = "${item.blockId}/${item.blockSubtypeId}"

                    override fun fromGroups(groups: MatchGroupCollection): Block.Companion.BlockId? = Block.Companion.BlockId(groups[1]?.value, groups[2]?.value)

                }){
            override fun getBlockValue(block: Block): MutableList<Block.Companion.BlockId> = block.blockVariants ?: arrayListOf()

            override fun setBlockValue(block: Block, newValue: MutableList<Block.Companion.BlockId>) {
                block.blockVariants = newValue
            }
        }

        //endregion
        //region Balance accessors

        inputControllers += object : FieldInputController<Int>("PCU", BlPCU, block, IntToStringConverter()){
            override fun getBlockValue(block: Block): Int = block.PCU

            override fun setBlockValue(block: Block, newValue: Int) {
                block.PCU = newValue
            }
        }

        inputControllers += object : FieldInputController<Long>("buildTimeSeconds", BlBuildTimeSeconds, block, LongToStringConverter()){
            override fun getBlockValue(block: Block): Long = block.buildTimeSeconds

            override fun setBlockValue(block: Block, newValue: Long) {
                block.buildTimeSeconds = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("DisassembleRatio", BlDisassembleRatio, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.DisassembleRatio

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.DisassembleRatio = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("DeformationRatio", BlDeformationRatio, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.DeformationRatio

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.DeformationRatio = newValue
            }
        }

        inputControllers += object : FieldInputController<Float>("InventoryMaxVolume", BlInventoryMaxVolume, block, FloatToStringConverter()){
            override fun getBlockValue(block: Block): Float = block.InventoryMaxVolume

            override fun setBlockValue(block: Block, newValue: Float) {
                block.InventoryMaxVolume = newValue
            }
        }

        inputControllers += object : FieldInputController<Float>("InventorySize.x", BlInventorySizeX, block, FloatToStringConverter()){
            override fun getBlockValue(block: Block): Float = block.InventorySize?.x ?: 0.0f

            override fun setBlockValue(block: Block, newValue: Float) {
                (block.InventorySize ?: Block.Companion.XYZFloat().apply { block.InventorySize = this }).x = newValue
            }
        }

        inputControllers += object : FieldInputController<Float>("InventorySize.y", BlInventorySizeY, block, FloatToStringConverter()){
            override fun getBlockValue(block: Block): Float = block.InventorySize?.y ?: 0.0f

            override fun setBlockValue(block: Block, newValue: Float) {
                (block.InventorySize ?: Block.Companion.XYZFloat().apply { block.InventorySize = this }).y = newValue
            }
        }

        inputControllers += object : FieldInputController<Float>("InventorySize.z", BlInventorySizeZ, block, FloatToStringConverter()){
            override fun getBlockValue(block: Block): Float = block.InventorySize?.z ?: 0.0f

            override fun setBlockValue(block: Block, newValue: Float) {
                (block.InventorySize ?: Block.Companion.XYZFloat().apply { block.InventorySize = this }).z = newValue
            }
        }

        data class EditorControllerBlockList(
                val components: MutableList<Block.Companion.ComponentItem>,
                val critical: MPair<String, Int>? = null
        )

        inputControllers += object : TextAreaInputController<EditorControllerBlockList>("BlockList", BlComponent, block, object : ABlockInputController.IConverter<EditorControllerBlockList, String> {
                    val regex = Regex("^(\\d+)? ([a-zA-Z0-9_]*)( -> ([a-zA-Z0-9_]*)\\/([a-zA-Z0-9_]*))?$")
                    val delim = "---"

                    override fun convert(value: EditorControllerBlockList): String {
                        val map = HashMap<String?, Int>()
                        return value.components.joinToString(separator = "\n") {
                            addDelimIfNeeded("${it.amount} ${it.component}${it.deconstructId?.let { " -> ${it.blockId}/${it.blockSubtypeId}" } ?: ""}", it, value.critical, map)
                        }
                    }

                    override fun reverce(value: String): EditorControllerBlockList {
                        //TODO if delimiter need positioned post component - save last component and make critical from him
                        val map = HashMap<String?, Int>()
                        var critical: MPair<String, Int>? = null
                        var last: Block.Companion.ComponentItem? = null
                        return value.lineSequence().mapNotNull {
                            if(it == delim){
                                critical = MPair(last!!.component!!, map[last!!.component]!!)
                                null
                            } else {
                                regex.find(it)?.groups?.let {
                                    Block.Companion.ComponentItem(it[1]?.value?.toIntOrNull() ?: 0, it[2]?.value, it[3]?.run {
                                        Block.Companion.BlockId(it[4]?.value, it[5]?.value)
                                    }).apply {
                                        incrementById(this, map)
                                        last = this
                                    }
                                }
                            }
                        }.toMutableList().let {
                            EditorControllerBlockList(it, critical)
                        }
                    }

                    inline fun addDelimIfNeeded(base: String, iter: Block.Companion.ComponentItem, critical: MPair<String, Int>?, countMap: MutableMap<String?, Int>): String{
                        //TODO if delimiter need positioned post component - replace base <-> (critical.let...) and delim + "\n" -> "\n" + delim
                        incrementById(iter, countMap)
                        return base + (critical?.let {
                            if (iter.component == critical.first && countMap[critical.first] == critical.second) {
                                "\n" + delim
                            } else {
                                null
                            }
                        } ?: "")
                    }

                    inline fun incrementById(iter: Block.Companion.ComponentItem, countMap: MutableMap<String?, Int>){
                        val idx = (countMap[iter.component] ?: -1) + 1
                        countMap[iter.component] = idx
                    }
                }){
            override fun getBlockValue(block: Block): EditorControllerBlockList = EditorControllerBlockList(block.component ?: arrayListOf(), block.criticalComponent)

            override fun setBlockValue(block: Block, newValue: EditorControllerBlockList) {
                block.component = newValue.components
                block.criticalComponent = newValue.critical
            }
        }
        //endregion
        //region Model accessors

        inputControllers += object : FieldInputController<String>("icon", BlIcon, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.icon ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.icon = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("model", BlModel, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.model ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                block.model = newValue
            }
        }

        inputControllers += object : TextAreaInputController<MutableList<Block.Companion.BuildProgressModellItem>>(
                "buildProgressModels",
                BlBuildProgressModels,
                block,
                object : LinesRegexConverter<Block.Companion.BuildProgressModellItem>(Regex("^(\\d+(\\.\\d+)?)? (.*)\$")){
            override fun toLine(item: Block.Companion.BuildProgressModellItem): String = "${item.progress} ${item.file ?: ""}"

            override fun fromGroups(groups: MatchGroupCollection): Block.Companion.BuildProgressModellItem? = Block.Companion.BuildProgressModellItem(groups[1]?.value?.toFloat() ?: 0.0f, groups[3]?.value)

        }){
            override fun getBlockValue(block: Block): MutableList<Block.Companion.BuildProgressModellItem> = block.buildProgressModels ?: arrayListOf()

            override fun setBlockValue(block: Block, newValue: MutableList<Block.Companion.BuildProgressModellItem>) {
                block.buildProgressModels = newValue
            }
        }

        inputControllers += object : CheckboxInputController("cubeSize", BlCubeSize, block){
            override fun getBlockValue(block: Block): Boolean = block.cubeSize.equals("Large", true)

            override fun setBlockValue(block: Block, newValue: Boolean) {
                block.cubeSize = if(newValue) "Large" else "Small"
            }
        }

        inputControllers += object : CheckboxInputController("isAirTight", BlIsAirTight, block){
            override fun getBlockValue(block: Block): Boolean = block.isAirTight

            override fun setBlockValue(block: Block, newValue: Boolean) {
                block.isAirTight = newValue
            }
        }

        inputControllers += object : FieldInputController<Int>("size.x", BlSizeX, block, IntToStringConverter()){
            override fun getBlockValue(block: Block): Int = block.size?.x ?: 0

            override fun setBlockValue(block: Block, newValue: Int) {
                (block.size ?: Block.Companion.XYZInt().apply { block.size = this }).x = newValue
            }
        }

        inputControllers += object : FieldInputController<Int>("size.y", BlSizeY, block, IntToStringConverter()){
            override fun getBlockValue(block: Block): Int = block.size?.y ?: 0

            override fun setBlockValue(block: Block, newValue: Int) {
                (block.size ?: Block.Companion.XYZInt().apply { block.size = this }).y = newValue
            }
        }

        inputControllers += object : FieldInputController<Int>("size.z", BlSizeZ, block, IntToStringConverter()){
            override fun getBlockValue(block: Block): Int = block.size?.z ?: 0

            override fun setBlockValue(block: Block, newValue: Int) {
                (block.size ?: Block.Companion.XYZInt().apply { block.size = this }).z = newValue
            }
        }

        inputControllers += object : FieldInputController<Float>("modelOffset.x", BlModelOffsetX, block, FloatToStringConverter()){
            override fun getBlockValue(block: Block): Float = block.modelOffset?.x ?: 0f

            override fun setBlockValue(block: Block, newValue: Float) {
                (block.modelOffset ?: Block.Companion.XYZFloat().apply { block.modelOffset = this }).x = newValue
            }
        }

        inputControllers += object : FieldInputController<Float>("modelOffset.y", BlModelOffsetY, block, FloatToStringConverter()){
            override fun getBlockValue(block: Block): Float = block.modelOffset?.y ?: 0f

            override fun setBlockValue(block: Block, newValue: Float) {
                (block.modelOffset ?: Block.Companion.XYZFloat().apply { block.modelOffset = this }).y = newValue
            }
        }

        inputControllers += object : FieldInputController<Float>("modelOffset.z", BlModelOffsetZ, block, FloatToStringConverter()){
            override fun getBlockValue(block: Block): Float = block.modelOffset?.z ?: 0f

            override fun setBlockValue(block: Block, newValue: Float) {
                (block.modelOffset ?: Block.Companion.XYZFloat().apply { block.modelOffset = this }).z = newValue
            }
        }

        inputControllers += object : FieldInputController<Int?>("center.x", BlCenterX, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.center?.x

            override fun setBlockValue(block: Block, newValue: Int?) {
                if(newValue != null || BlCenterY.text.isNotBlank() || BlCenterZ.text.isNotBlank()){
                    (block.center ?: Block.Companion.XYZInt().apply { block.center = this }).x = newValue ?: 0
                } else {
                    block.center = null
                }
            }
        }

        inputControllers += object : FieldInputController<Int?>("center.y", BlCenterY, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.center?.y

            override fun setBlockValue(block: Block, newValue: Int?) {
                if(newValue != null || BlCenterX.text.isNotBlank() || BlCenterZ.text.isNotBlank()){
                    (block.center ?: Block.Companion.XYZInt().apply { block.center = this }).y = newValue ?: 0
                } else {
                    block.center = null
                }
            }
        }

        inputControllers += object : FieldInputController<Int?>("center.z", BlCenterZ, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.center?.z

            override fun setBlockValue(block: Block, newValue: Int?) {
                if(newValue != null || BlCenterX.text.isNotBlank() || BlCenterY.text.isNotBlank()){
                    (block.center ?: Block.Companion.XYZInt().apply { block.center = this }).z = newValue ?: 0
                } else {
                    block.center = null
                }
            }
        }

        inputControllers += object : FieldInputController<String>("mirroring.x", BlMirroringX, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.mirroring?.x ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                (block.mirroring ?: Block.Companion.XYZAxis().apply { block.mirroring = this }).x = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("mirroring.y", BlMirroringY, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.mirroring?.y ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                (block.mirroring ?: Block.Companion.XYZAxis().apply { block.mirroring = this }).y = newValue
            }
        }

        inputControllers += object : FieldInputController<String>("mirroring.z", BlMirroringZ, block, IdentityConverter()){
            override fun getBlockValue(block: Block): String = block.mirroring?.z ?: ""

            override fun setBlockValue(block: Block, newValue: String) {
                (block.mirroring ?: Block.Companion.XYZAxis().apply { block.mirroring = this }).z = newValue
            }
        }

        inputControllers += object : TextAreaInputController<MutableList<Block.Companion.MountPointItem>>(
                "mountPoints",
                BlMountPoints,
                block,
                object : LinesRegexConverter<Block.Companion.MountPointItem>(Regex("^(Right|Left|Top|Bottom|Front|Back) sx=(\\d+(\\.\\d+)?)? sy=(\\d+(\\.\\d+)?)? ex=(\\d+(\\.\\d+)?)? ey=(\\d+(\\.\\d+)?)?( \\+)?\$")){
                    override fun toLine(item: Block.Companion.MountPointItem): String = "${item.side} sx=${item.startX} sy=${item.startY} ex=${item.endX} ey=${item.endY}${if(item.default) " +" else ""}"

                    override fun fromGroups(groups: MatchGroupCollection): Block.Companion.MountPointItem? = Block.Companion.MountPointItem(groups[1]?.value!!, groups[2]?.value?.toFloat() ?: 0.0f, groups[4]?.value?.toFloat() ?: 0.0f, groups[6]?.value?.toFloat() ?: 0.0f, groups[8]?.value?.toFloat() ?: 0.0f, groups[10]?.value == " +")

                }){
            override fun getBlockValue(block: Block): MutableList<Block.Companion.MountPointItem> = block.mountPoints ?: arrayListOf()

            override fun setBlockValue(block: Block, newValue: MutableList<Block.Companion.MountPointItem>) {
                block.mountPoints = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("blockTopologyId", BlBlockTopology, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.blockTopologyId

            override fun setBlockValue(block: Block, newValue: String?) {
                block.blockTopologyId = newValue
            }
        }
        /*inputControllers += object : ComboBoxInputController<String, BlockTopologyItem>(BlBlockTopology, block){
            override fun getBlockValue(block: Block): BlockTopologyItem {
                val id = block.blockTopologyId ?: ""
                return findItem(id) ?: BlockTopologyItem(block.blockTopologyId ?: "", "")
            }

            override fun setBlockValue(block: Block, newValue: BlockTopologyItem) {
                block.blockTopologyId = newValue.id
            }

            override fun getId(value: BlockTopologyItem): String = value.id
        }*/

        inputControllers += object : FieldInputController<Int?>("DamageEffectId", BlDamageEffectId, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.DamageEffectId

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.DamageEffectId = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("damageEffectName", BlDamageEffectName, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.damageEffectName

            override fun setBlockValue(block: Block, newValue: String?) {
                block.damageEffectName = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("damagedSound", BlDamagedSound, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.damagedSound

            override fun setBlockValue(block: Block, newValue: String?) {
                block.damagedSound = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("primarySound", BlPrimarySound, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.primarySound

            override fun setBlockValue(block: Block, newValue: String?) {
                block.primarySound = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("actionSound", BlActionSound, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.actionSound

            override fun setBlockValue(block: Block, newValue: String?) {
                block.actionSound = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("destroyEffect", BlDestroyEffect, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.destroyEffect

            override fun setBlockValue(block: Block, newValue: String?) {
                block.destroyEffect = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("destroySound", BlDestroySound, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.destroySound

            override fun setBlockValue(block: Block, newValue: String?) {
                block.destroySound = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("GenerateSound", BlGenerateSound, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.GenerateSound

            override fun setBlockValue(block: Block, newValue: String?) {
                block.GenerateSound = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("IdleSound", BlIdleSound, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.IdleSound

            override fun setBlockValue(block: Block, newValue: String?) {
                block.IdleSound = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("OverlayTexture", BlOverlayTexture, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.OverlayTexture

            override fun setBlockValue(block: Block, newValue: String?) {
                block.OverlayTexture = newValue
            }
        }

        inputControllers += object : CheckboxInputController("UseModelIntersection", BlUseModelIntersection, block){
            override fun getBlockValue(block: Block): Boolean = block.UseModelIntersection

            override fun setBlockValue(block: Block, newValue: Boolean) {
                block.UseModelIntersection = newValue
            }
        }

        inputControllers += object : FieldInputController<String?>("AutorotateMode", BlAutorotateMode, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.AutorotateMode

            override fun setBlockValue(block: Block, newValue: String?) {
                block.AutorotateMode = newValue
            }
        }

        //endregion
        //region Extra accessors

        inputControllers += object : FieldInputController<Float?>("RequiredPowerInput", BlRequiredPowerInput, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.RequiredPowerInput

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.RequiredPowerInput = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("BasePowerInput", BlBasePowerInput, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.BasePowerInput

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.BasePowerInput = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("ConsumptionPower", BlConsumptionPower, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.ConsumptionPower

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.ConsumptionPower = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("MaxPowerOutput", BlMaxPowerOutput, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MaxPowerOutput

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MaxPowerOutput = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("StandbyPowerConsumption", BlStandByPowerConsumption, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.StandbyPowerConsumption

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.StandbyPowerConsumption = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("OperationalPowerConsumption", BlOperationalPowerConsumption, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.OperationalPowerConsumption

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.OperationalPowerConsumption = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("PowerInput", BlPowerInput, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.PowerInput

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.PowerInput = newValue
            }
        }

        inputControllers += object : FieldInputController<Float?>("PowerConsumptionIdle", BlPowerConsumptionIdle, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.PowerConsumptionIdle

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.PowerConsumptionIdle = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("PowerConsumptionMoving", BlPowerConsumptionMoving, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.PowerConsumptionMoving

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.PowerConsumptionMoving = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MaxPowerConsumption", BlMaxPowerConsumption, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MaxPowerConsumption

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MaxPowerConsumption = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MinPowerConsumption", BlMinPowerConsumption, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MinPowerConsumption

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MinPowerConsumption = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MaxStoredPower", BlMaxStoredPower, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MaxStoredPower

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MaxStoredPower = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("InitialStoredPowerRatio", BlInitialStoredPowerRatio, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.InitialStoredPowerRatio

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.InitialStoredPowerRatio = newValue
            }
        }
        //endregion
        //region Thrusters accessors
        inputControllers += object : ColorpickerInputController("FlameFullColor", BlFlameFullColorRGBO, block){
            override fun getBlockValue(block: Block): Color = block.FlameFullColor ?: emptyColor

            override fun setBlockValue(block: Block, newValue: Color) {
                block.FlameFullColor = newValue.takeIf { it != emptyColor }
            }
        }
        inputControllers += object : ColorpickerInputController("FlameIdleColor", BlFlameIdleColorRGBO, block){
            override fun getBlockValue(block: Block): Color = block.FlameIdleColor ?: emptyColor

            override fun setBlockValue(block: Block, newValue: Color) {
                block.FlameIdleColor = newValue.takeIf { it != emptyColor }
            }
        }

        inputControllers += object : FieldInputController<Float?>("FlameDamageLengthScale", BlFlameDamageLengthScale, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.FlameDamageLengthScale

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.FlameDamageLengthScale = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("FlameLengthScale", BlFlameLengthScale, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.FlameLengthScale

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.FlameLengthScale = newValue
            }
        }
        inputControllers += object : CheckboxInputController("NeedsAtmosphereForInfluence", BlNeedsAtmosphereForInfluence, block){
            override fun getBlockValue(block: Block): Boolean = block.NeedsAtmosphereForInfluence ?: false

            override fun setBlockValue(block: Block, newValue: Boolean) {
                block.NeedsAtmosphereForInfluence = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MinPlanetaryInfluence", BlMinPlanetaryInfluence, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MinPlanetaryInfluence

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MinPlanetaryInfluence = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MaxPlanetaryInfluence", BlMaxPlanetaryInfluence, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MaxPlanetaryInfluence

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MaxPlanetaryInfluence = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("EffectivenessAtMinInfluence", BlEffectivenessAtMinInfluence, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.EffectivenessAtMinInfluence

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.EffectivenessAtMinInfluence = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("EffectivenessAtMaxInfluence", BlEffectivenessAtMaxInfluence, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.EffectivenessAtMaxInfluence

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.EffectivenessAtMaxInfluence = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("SlowdownFactor", BlSlowdownFactor, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.SlowdownFactor

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.SlowdownFactor = newValue
            }
        }
        inputControllers += object : FieldInputController<String?>("FlamePointMaterial", BlFlamePointMaterial, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.FlamePointMaterial

            override fun setBlockValue(block: Block, newValue: String?) {
                block.FlamePointMaterial = newValue
            }
        }
        inputControllers += object : FieldInputController<String?>("FlameLengthMaterial", BlFlameLengthMaterial, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.FlameLengthMaterial

            override fun setBlockValue(block: Block, newValue: String?) {
                block.FlameLengthMaterial = newValue
            }
        }
        inputControllers += object : FieldInputController<String?>("FlameFlare", BlFlameFlare, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.FlameFlare

            override fun setBlockValue(block: Block, newValue: String?) {
                block.FlameFlare = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("FlameVisibilityDistance", BlFlameVisibilityDistance, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.FlameVisibilityDistance

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.FlameVisibilityDistance = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("FlameGlareQuerySize", BlFlameGlareQuerySize, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.FlameGlareQuerySize

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.FlameGlareQuerySize = newValue
            }
        }
        inputControllers += object : FieldInputController<Double?>("ForceMagnitude", BlForceMagnitude, block, DoubleToStringNullableConverter()){
            override fun getBlockValue(block: Block): Double? = block.ForceMagnitude

            override fun setBlockValue(block: Block, newValue: Double?) {
                block.ForceMagnitude = newValue
            }
        }
        //endregion
        //region Specials accessors
        inputControllers += object : FieldInputController<Int?>("PowerNeededForJump", BlPowerNeededForJump, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.PowerNeededForJump

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.PowerNeededForJump = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("MaxJumpDistance", BlMaxJumpDistance, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.MaxJumpDistance

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.MaxJumpDistance = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("MaxJumpMass", BlMaxJumpMass, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.MaxJumpMass

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.MaxJumpMass = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("RefineSpeed", BlRefineSpeed, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.RefineSpeed

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.RefineSpeed = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MaterialEfficiency", BlMaterialEfficiency, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MaterialEfficiency

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MaterialEfficiency = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("SensorRadius", BlSensorRadius, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.SensorRadius

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.SensorRadius = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("SensorOffset", BlSensorOffset, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.SensorOffset

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.SensorOffset = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("CutOutRadius", BlCutOutRadius, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.CutOutRadius

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.CutOutRadius = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("CutOutOffset", BlCutOutOffset, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.CutOutOffset

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.CutOutOffset = newValue
            }
        }
        inputControllers += object : FieldInputController<String?>("EmissiveColorPreset", BlEmissiveColorPreset, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.EmissiveColorPreset

            override fun setBlockValue(block: Block, newValue: String?) {
                block.EmissiveColorPreset = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("OreAmountPerPullRequest", BlOreAmountPerPullRequest, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.OreAmountPerPullRequest

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.OreAmountPerPullRequest = newValue
            }
        }
        inputControllers += object : FieldInputController<Long?>("Capacity", BlCapacity, block, LongToStringNullableConverter()){
            override fun getBlockValue(block: Block): Long? = block.Capacity

            override fun setBlockValue(block: Block, newValue: Long?) {
                block.Capacity = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("MaximumRange", BlMaximumRange, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.MaximumRange

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.MaximumRange = newValue
            }
        }
        //endregion
        //region Weap accessors
        inputControllers += object : FieldInputController<String?>("WeaponDefinitionId", BlWeaponDefinitionId, block, StringToStringNullableConverter()){
            override fun getBlockValue(block: Block): String? = block.WeaponDefinitionId

            override fun setBlockValue(block: Block, newValue: String?) {
                block.WeaponDefinitionId = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("MinElevationDegrees", BlMinElevationDegrees, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.MinElevationDegrees

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.MinElevationDegrees = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("MaxElevationDegrees", BlMaxElevationDegrees, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.MaxElevationDegrees

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.MaxElevationDegrees = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("MinAzimuthDegrees", BlMinAzimuthDegrees, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.MinAzimuthDegrees

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.MinAzimuthDegrees = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("MaxAzimuthDegrees", BlMaxAzimuthDegrees, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.MaxAzimuthDegrees

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.MaxAzimuthDegrees = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("RotationSpeed", BlRotationSpeed, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.RotationSpeed

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.RotationSpeed = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("ElevationSpeed", BlElevationSpeed, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.ElevationSpeed

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.ElevationSpeed = newValue
            }
        }
        inputControllers += object : CheckboxInputController("IdleRotation", BlIdleRotation, block){
            override fun getBlockValue(block: Block): Boolean = block.IdleRotation ?: false

            override fun setBlockValue(block: Block, newValue: Boolean) {
                block.IdleRotation = true
            }
        }
        inputControllers += object : FieldInputController<Int?>("MaxRangeMeters", BlMaxRangeMeters, block, IntToStringNullableConverter()){
            override fun getBlockValue(block: Block): Int? = block.MaxRangeMeters

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.MaxRangeMeters = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MinFov", BlMinFov, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MinFov

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MinFov = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MaxFov", BlMaxFov, block, FloatToStringNullableConverter()){
            override fun getBlockValue(block: Block): Float? = block.MaxFov

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MaxFov = newValue
            }
        }
        //endregion
        //region Wheels accessors
        inputControllers += object : FieldInputController<Float?>("MaxForceMagnitude", BlMaxForceMagnitude, block, FloatToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Float? = block.MaxForceMagnitude

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MaxForceMagnitude = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("DangerousTorqueThreshold", BlDangerousTorqueThreshold, block, FloatToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Float? = block.DangerousTorqueThreshold

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.DangerousTorqueThreshold = newValue
            }
        }
        inputControllers += object : FieldInputController<String?>("RotorPart", BlRotorPart, block, StringToStringNullableConverter()) {
            override fun getBlockValue(block: Block): String? = block.RotorPart

            override fun setBlockValue(block: Block, newValue: String?) {
                block.RotorPart = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("PropulsionForce", BlPropulsionForce, block, IntToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Int? = block.PropulsionForce

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.PropulsionForce = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MinHeight", BlMinHeight, block, FloatToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Float? = block.MinHeight

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MinHeight = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("MaxHeight", BlMaxHeight, block, FloatToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Float? = block.MaxHeight

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.MaxHeight = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("SafetyDetach", BlSafetyDetach, block, IntToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Int? = block.SafetyDetach

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.SafetyDetach = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("SafetyDetachMax", BlSafetyDetachMax, block, IntToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Int? = block.SafetyDetachMax

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.SafetyDetachMax = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("AxleFriction", BlAxleFriction, block, IntToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Int? = block.AxleFriction

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.AxleFriction = newValue
            }
        }
        inputControllers += object : FieldInputController<Float?>("SteeringSpeed", BlSteeringSpeed, block, FloatToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Float? = block.SteeringSpeed

            override fun setBlockValue(block: Block, newValue: Float?) {
                block.SteeringSpeed = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("AirShockMinSpeed", BlAirShockMinSpeed, block, IntToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Int? = block.AirShockMinSpeed

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.AirShockMinSpeed = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("AirShockMaxSpeed", BlAirShockMaxSpeed, block, IntToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Int? = block.AirShockMaxSpeed

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.AirShockMaxSpeed = newValue
            }
        }
        inputControllers += object : FieldInputController<Int?>("AirShockActivationDelay", BlAirShockActivationDelay, block, IntToStringNullableConverter()) {
            override fun getBlockValue(block: Block): Int? = block.AirShockActivationDelay

            override fun setBlockValue(block: Block, newValue: Int?) {
                block.AirShockActivationDelay = newValue
            }
        }
        //endregion
    }

    fun changeBlock(block: Block){
        this.block.value = block
    }

    fun getBlock() = this.block.value

    fun blockExternalChanged(){
        for (controller in inputControllers) {
            controller.blockExternalChanged()
        }
    }

    abstract class ABlockInputController<T, I>(
            val name: String,
            val block: ObjectProperty<Block>,
            val converter: IConverter<T, I>
    ){
        private var initialize: Boolean = false

        init {
            block.addListener { _, oldValue, newValue ->
                if(oldValue != newValue){
                    fireChange(newValue)
                }
            }
        }

        fun blockExternalChanged(){
            fireChange(block.value)
        }

        //change value with external block
        fun applyExtBlockChange(block: Block, newValue: I){
            try{
                converter.reverce(newValue)
            } catch (e: ParseException){
                println("${e.message} from block ${block.blockId?.let { it.blockSubtypeId.takeUnless { it.isNullOrBlank() } ?: it.blockId }} with field $name")
                return
            }.let {
                setBlockValue(block, it)
            }
        }

        fun getExtBlockValue(block: Block): I? {
            return converter.convert(getBlockValue(block))
        }

        abstract fun getInputType(): Class<I>

        private fun fireChange(newBlock: Block){
            initialize = true
            try {
                change(converter.convert(getBlockValue(newBlock)))
            } catch (e: ParseException){
                print(e.message)
            } finally {
                initialize = false
            }
        }

        protected abstract fun change(newValue: I)

        protected fun onChange(newValue: I){
            if(!initialize){
                try{
                    converter.reverce(newValue)
                } catch (e: ParseException){
                    println(e.message)
                    return
                }.let {
                    setBlockValue(block.value, it)
                }
            }
        }

        protected abstract fun getBlockValue(block: Block): T

        protected abstract fun setBlockValue(block: Block, newValue: T)

        interface IConverter<T, I>{
            fun convert(value: T): I
            fun reverce(value: I): T
        }

    }

    abstract class CheckboxInputController(
            name: String,
            val checkbox: CheckBox,
            block: ObjectProperty<Block>
    ): ABlockInputController<Boolean, Boolean>(name, block, IdentityConverter()){
        init {
            checkbox.setOnAction {
                onChange(checkbox.isSelected)
            }
        }

        override fun change(newValue: Boolean) {
            checkbox.isSelected = newValue
        }

        override fun getInputType(): Class<Boolean> {
            return Boolean::class.java
        }
    }

    abstract class ColorpickerInputController(
            name: String,
            val colorPicker: ColorPicker,
            block: ObjectProperty<Block>
    ): ABlockInputController<Color, Color>(name, block, IdentityConverter()){
        init {
            colorPicker.setOnAction {
                onChange(colorPicker.value)
            }
        }

        override fun change(newValue: Color) {
            colorPicker.value = newValue
        }

        override fun getInputType(): Class<Color> {
            return Color::class.java
        }
    }

    abstract class FieldInputController<T>(
            name: String,
            val field: TextField,
            block: ObjectProperty<Block>,
            converter: IConverter<T, String>
    ): ABlockInputController<T, String>(name, block, converter){
        init {
            field.setOnAction {
                onChange(field.text)
            }
            field.textProperty().addListener { _, _, newValue -> //??? (Без этого значение сохраняется только после нажатия "ENTER")
                onChange(newValue)
            }
        }

        override fun change(newValue: String) {
            field.text = newValue
        }

        override fun getInputType(): Class<String> {
            return String::class.java
        }
    }

    abstract class TextAreaInputController<T>(
            name: String,
            val field: TextArea,
            block: ObjectProperty<Block>,
            converter: IConverter<T, String>
    ): ABlockInputController<T, String>(name, block, converter){
        init {
            field.textProperty().addListener{ _, _, newValue ->
                onChange(newValue)
            }
        }

        override fun change(newValue: String) {
            field.text = newValue
        }

        override fun getInputType(): Class<String> {
            return String::class.java
        }
    }

    abstract class CodeAreaInputController<T>(
            name: String,
            val field: CodeArea,
            block: ObjectProperty<Block>,
            converter: IConverter<T, String>
    ): ABlockInputController<T, String>(name, block, converter){
        init {
            field.textProperty().addListener{ _, _, newValue ->
                onChange(newValue)
            }
        }

        override fun change(newValue: String) {
            field.replaceText(0, field.length, newValue)
        }

        override fun getInputType(): Class<String> {
            return String::class.java
        }
    }

    abstract class ComboBoxInputController<ID, T>(
            name: String,
            val field: ComboBox<T>,
            block: ObjectProperty<Block>
    ): ABlockInputController<T, T>(name, block, IdentityConverter()){
        init {
            field.setOnAction {
                onChange(field.value)
            }
        }

        override fun change(newValue: T) {
            field.value = findItem(getId(newValue))
        }

        protected fun findItem(id: ID): T? = field.items.find { getId(it) == id }

        protected abstract fun getId(value: T): ID
    }

    class IdentityConverter<T>(): ABlockInputController.IConverter<T, T> {
        override fun convert(value: T): T = value

        override fun reverce(value: T): T = value
    }

    class StringToStringNullableConverter(val nullValue: String = ""): ABlockInputController.IConverter<String?, String>{
        override fun convert(value: String?): String = value ?: nullValue

        override fun reverce(value: String): String? = value.takeIf { it != nullValue }
    }

    class IntToStringConverter(): ABlockInputController.IConverter<Int, String>{
        override fun convert(value: Int): String = value.toString()

        override fun reverce(value: String): Int = value.toIntOrNull() ?: throw ParseException("cannot convert $value to int", 0)
    }

    class IntToStringNullableConverter(): ABlockInputController.IConverter<Int?, String>{
        override fun convert(value: Int?): String = value?.toString() ?: ""

        override fun reverce(value: String): Int? = if(value.isBlank()) null else (value.toIntOrNull() ?: throw ParseException("cannot convert $value to int", 0))
    }

    class LongToStringConverter(): ABlockInputController.IConverter<Long, String>{
        override fun convert(value: Long): String = value.toString()

        override fun reverce(value: String): Long = value.toLongOrNull() ?: throw ParseException("cannot convert $value to long", 0)
    }

    class LongToStringNullableConverter(): ABlockInputController.IConverter<Long?, String>{
        override fun convert(value: Long?): String = value?.toString() ?: ""

        override fun reverce(value: String): Long? = if(value.isBlank()) null else (value.toLongOrNull() ?: throw ParseException("cannot convert $value to long", 0))
    }

    class FloatToStringConverter(): ABlockInputController.IConverter<Float, String>{
        override fun convert(value: Float): String = value.toString()

        override fun reverce(value: String): Float = value.toFloatOrNull() ?: throw ParseException("cannot convert $value to float", 0)
    }

    class FloatToStringNullableConverter(): ABlockInputController.IConverter<Float?, String>{
        override fun convert(value: Float?): String = value?.toString() ?: ""

        override fun reverce(value: String): Float? = if(value.isBlank()) null else (value.toFloatOrNull() ?: throw ParseException("cannot convert $value to float", 0))
    }

    class DoubleToStringNullableConverter(): ABlockInputController.IConverter<Double?, String>{
        override fun convert(value: Double?): String = value?.toString() ?: ""

        override fun reverce(value: String): Double? = if(value.isBlank()) null else (value.toDoubleOrNull() ?: throw ParseException("cannot convert $value to double", 0))
    }

    abstract class LinesRegexConverter<I: Any>(val reg: Regex): ABlockInputController.IConverter<MutableList<I>, String>{
        override fun convert(value: MutableList<I>): String = value.joinToString(separator = "\n") { toLine(it) }

        override fun reverce(value: String): MutableList<I> {
            return value.lineSequence().mapNotNull {
                reg.find(it)?.groups?.let {
                    fromGroups(it)
                }
            }.toMutableList()
        }

        protected abstract fun toLine(item: I): String

        protected abstract fun fromGroups(groups: MatchGroupCollection): I?
    }

    companion object {
        val emptyColor = Color.rgb(0, 0, 0, 0.0)
    }
}
