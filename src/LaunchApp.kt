import com.sun.javafx.application.LauncherImpl
import sample.Main

fun main(args: Array<String>) {
    LauncherImpl.launchApplication(Main::class.java, args)
}